import { NgModule, LOCALE_ID } from '@angular/core';
import { FuseModule } from './@fuse/fuse.module';
import { fuseConfig } from './@common/fuse-config';
import { FuseProgressBarModule } from './@fuse/components/progress-bar/progress-bar.module';
import { FuseSharedModule } from './@fuse/shared.module';
import { FuseSidebarModule } from './@fuse/components/sidebar/sidebar.module';
import { FuseThemeOptionsModule } from './@fuse/components/theme-options/theme-options.module';
import { PublicPagesModule } from './@pages/public/public-pages.module';
import { PrivatePagesModule } from './@pages/private/private-pages.module';
import { CpExpertCommonModule } from './@common/cpexpert-common.module';
import { FuseNavigationService } from './@fuse/components/navigation/navigation.service';
import { FuseTranslationLoaderService } from './@fuse/services/translation-loader.service';
import { FuseNavigation } from './@fuse/types/fuse-navigation';
import { LanguagesEnum } from './@common/enums/languages.enum';
import { TranslateService, TranslateModule } from '@ngx-translate/core';
import { Utils } from './@common/utils';
import { SystemUserTypesEnum } from './@common/enums/system-user-types.enum';
import { AuthService } from './@common/services/http/auth.service';
import { NGXLogger } from 'ngx-logger';
import { Route } from '@angular/router';
import { registerLocaleData } from '@angular/common';
import localePolish from '@angular/common/locales/pl';
import localeEnglish from '@angular/common/locales/en';
import { OrganizationsService } from './@common/services/http/organizations.service';
import { AuthResponseModel } from './@common/models/auth-response.model';
import { IResponseInterface } from './@common/interfaces/i-response.interface';
import { UserModule } from './@pages/private/user/user.module';

registerLocaleData(localePolish, localeEnglish);

@NgModule({
    declarations: [],
    imports: [
        FuseModule.forRoot(fuseConfig)
    ],
    providers: [{ provide: LOCALE_ID, useValue: 'pl' }],
    bootstrap: [],
    exports: [
        FuseModule,
        FuseProgressBarModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseThemeOptionsModule,
        PublicPagesModule,
        PrivatePagesModule,
        CpExpertCommonModule,
        TranslateModule
    ]
})
export class CpExpertCoreModule {

    public static Routes: Route[] = [...PrivatePagesModule.Routes, ...PublicPagesModule.Routes];

    constructor(
        private _fuseNavigationService: FuseNavigationService,
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private _translateService: TranslateService,
        private _authService: AuthService,
        private _organizationService: OrganizationsService,
        private _logger: NGXLogger
    ) {
        this._fuseNavigationService.unregister('main');
        this._fuseTranslationLoaderService.loadTranslations();
        this._translateService.addLangs([LanguagesEnum.English, LanguagesEnum.Polish]);
        this._translateService.setDefaultLang(LanguagesEnum.English);
        switch (this._translateService.getBrowserLang()) {
            case 'en':
                this._translateService.use(LanguagesEnum.English);
                break;
            case 'pl':
                this._translateService.use(LanguagesEnum.Polish);
                break;
            default:
                this._translateService.use(LanguagesEnum.English);
                break;
        }

        this._authService.loginSuccessEvent.subscribe((event: AuthResponseModel) => {
            const userType: SystemUserTypesEnum = Utils.GetCurrentUserType();
            const userOrganizationAppKey: string = Utils.GetCurrentUserOrganizationAppKey();
            if (userType != SystemUserTypesEnum.Undefined) {
                this._fuseTranslationLoaderService.loadTranslations(english, polish);
                this._fuseNavigationService.register('main', []);
                this._fuseNavigationService.setCurrentNavigation('main');
                this._fuseNavigationService.addNavigationItem(administrationNavigation, 'start');
                this._fuseNavigationService.updateNavigationItem('administration', { hidden: userType == SystemUserTypesEnum.User })
                this._fuseNavigationService.updateNavigationItem('administration-myorganization', { hidden: userType != SystemUserTypesEnum.Administrator })
                this._fuseNavigationService.updateNavigationItem('administration-configurations', { hidden: userType != SystemUserTypesEnum.SuperAdministrator })       
                let organizationsAppKeys = [];
                if(userType == SystemUserTypesEnum.SuperAdministrator) {
                    this._organizationService.getAllOrganizations().subscribe((response: IResponseInterface) => {
                        if (response && response.ResponseData && Array.isArray(response.ResponseData)) {
                            response.ResponseData.forEach(element => {
                                if (!organizationsAppKeys.find(x => x === element.AppKey)) {
                                    organizationsAppKeys.push(element.AppKey);
                                    this._fuseNavigationService.addNavigationItem({
                                        id: 'administration-configurations-organizations' + element.AppKey,
                                        title: element.Name,
                                        type: 'item',
                                        icon: 'apps',
                                        url: '/organization/' + element.AppKey
                                    }, 'administration-configurations-organizations')
                                } else {
                                    this._logger.warn('Organization: ' + element.AppKey + ' already added!');
                                }
                            });
                        }
                    });
                }
            } else {
                this._logger.warn('USER UNDEFINED TYPE!');
            }
        })

        this._authService.logoutSuccessEvent.subscribe(() => {
            this._logger.warn('User logout -> try unregister main menu!');
            this._fuseNavigationService.unregister('main');
        })
    }
}

const administrationNavigation: FuseNavigation = {
    id: 'administration',
    title: 'Administration',
    translate: 'Administration.Title',
    type: 'group',
    icon: 'apps',
    hidden: true,
    children: [
        {
            id: 'administration-myorganization',
            title: 'My organization',
            translate: 'Administration.MyOrganization.Title',
            type: 'collapsable',
            icon: 'dashboard',
            hidden: true,
            children: [
                {
                    id: 'administration-myorganization-users',
                    title: 'Users',
                    translate: 'Administration.MyOrganization.Users.Title',
                    type: 'item',
                    icon: 'groups',
                    url: UserModule.NavigationLinks.UserList
                },
                {
                    id: 'administration-myorganization-permisssions',
                    title: 'Permissions',
                    translate: 'Administration.MyOrganization.Permissions.Title',
                    type: 'item',
                    icon: 'https',
                    url: '/#'
                }
            ]
        },
        {
            id: 'administration-configurations',
            title: 'Configurations',
            translate: 'Administration.Configurations.Title',
            type: 'collapsable',
            icon: 'settings',
            hidden: true,
            children: [
                {
                    id: 'administration-configurations-organizations',
                    title: 'Organizations',
                    translate: 'Administration.Configurations.Organizations.Title',
                    type: 'collapsable',
                    icon: 'dashboard',
                    children: []
                },
                {
                    id: 'administration-configurations-system-params',
                    title: 'System parameters',
                    translate: 'Administration.Configurations.SystemParams.Title',
                    type: 'item',
                    icon: 'build',
                    url: '/#'
                },
                {
                    id: 'administration-configurations-system-modules',
                    title: 'Modules',
                    translate: 'Administration.Configurations.SystemModules.Title',
                    type: 'item',
                    icon: 'tab',
                    url: '/#'
                }
            ]
        }
    ]
};

const english = {
    lang: LanguagesEnum.English,
    data: {
        Administration: {
            Title: 'Administration',
            MyOrganization: {
                Title: 'My organization',
                Users: {
                    Title: 'Users',
                },
                Permissions: {
                    Title: 'Permissions'
                }
            },
            Configurations: {
                Title: 'Configurations',
                Organizations: {
                    Title: 'Organizations',
                },
                SystemParams: {
                    Title: 'System parameters',
                },
                SystemModules: {
                    Title: 'System modules',
                }
            }
        }
    }
}

const polish = {
    lang: LanguagesEnum.Polish,
    data: {
        Administration: {
            Title: 'Administracja',
            MyOrganization: {
                Title: 'Moja organizacja',
                Users: {
                    Title: 'Użytkownicy',
                },
                Permissions: {
                    Title: 'Uprawnienia'
                }
            },
            Configurations: {
                Title: 'Konfiguracja',
                Organizations: {
                    Title: 'Organizacje',
                },
                SystemParams: {
                    Title: 'Parametry systemowe',
                },
                SystemModules: {
                    Title: 'Moduły',
                }
            }
        }
    }
}
