import { Injectable } from '@angular/core';
import {
    HttpEvent,
    HttpRequest,
    HttpHandler,
    HttpInterceptor,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { NGXLogger } from 'ngx-logger';
import { Utils } from '../utils';

@Injectable()
export class RequestInterceptor implements HttpInterceptor {
    constructor(private _logger: NGXLogger) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        this._logger.debug('[RequestInterceptor].intercept(...)');
        request = request.clone({
            setHeaders: {
                Authorization: `Bearer ${Utils.GetCurrentUserToken()}`
            }
        });
        this._logger.debug('[RequestInterceptor].intercept(...).IRequest: ', request);
        return next.handle(request);
    }
}
