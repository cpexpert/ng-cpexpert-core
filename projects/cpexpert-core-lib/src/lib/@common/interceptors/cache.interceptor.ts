import { Injectable } from '@angular/core';
import {
    HttpEvent,
    HttpRequest,
    HttpHandler,
    HttpInterceptor,
    HttpResponse,
    HttpEventType
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { NGXLogger } from 'ngx-logger';
import { IRequestInterface } from '../interfaces/i-request.interface';
import { tap } from 'rxjs/operators';
import { IResponseInterface } from '../interfaces/i-response.interface';

@Injectable()
export class CacheInterceptor implements HttpInterceptor {

    constructor(private _logger: NGXLogger) {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        this._logger.debug('[CacheInterceptor].intercept(...)');
        if (req && req.body) {
            const request = req.body as IRequestInterface;
            if (request && request.CanCache) {
                return next.handle(req)
                .pipe(
                    tap(fullResponse => {
                        try {
                            if (fullResponse instanceof HttpResponse && fullResponse.body && fullResponse.body as IResponseInterface) {
                                this._logger.debug('[CacheInterceptor].intercept(...).fullResponse: ', fullResponse);
                                const response = fullResponse.body as IResponseInterface;
                                if (response.CanCache && response.IsSuccess && response.ResponseData) {
                                    // only canCache && success data cache
                                    this._logger.debug('[CacheInterceptor].intercept(...).CACHE_DATA');
                                }
                            } else {
                                throw new Error('[CacheInterceptor].intercept(...): HttpResponse or IResponse CAST problem');
                            }
                        } catch (error) {
                            this._logger.error(error);
                        }
                    }));
            }
        }
        return next.handle(req);
    }
}
