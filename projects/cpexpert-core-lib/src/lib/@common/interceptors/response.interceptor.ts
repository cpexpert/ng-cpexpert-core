import { Injectable } from '@angular/core';
import { HttpEvent, HttpRequest, HttpHandler, HttpInterceptor, HttpResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { NGXLogger } from 'ngx-logger';
import { ToastrService } from 'ngx-toastr';
import { tap, catchError } from 'rxjs/operators';
import { IResponseInterface } from '../interfaces/i-response.interface';
import { MessageTypesEnum } from '../enums/message-types.enum';
import { Utils } from '../utils';
import { AuthService } from '../services/http/auth.service';
import { TranslateService } from '@ngx-translate/core';
import { LanguagesEnum } from '../enums';

@Injectable()
export class ResponseInterceptor implements HttpInterceptor {

    constructor(private _logger: NGXLogger, private _toaster: ToastrService, private _authService: AuthService, private _translateService: TranslateService) { }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        this._logger.debug('[ResponseInterceptor].intercept(...)');
        return next.handle(request)
            .pipe(
                catchError(exception => {
                    if ([401, 403].indexOf(exception.status) !== -1) {
                        this._authService.logout();
                        this._logger.warn('[ResponseInterceptor].intercept(...): User logged out');
                        return;
                    }

                    const errors = exception.body && exception.body.Messages.filter(f => f.Type === MessageTypesEnum.Error) ||
                        exception.error && exception.error.message ||
                        exception.statusText;

                    if (Array.isArray(errors)) {
                        errors.forEach(error => {
                            this._toaster.error(error.Message);
                        });
                    } else {
                        this._toaster.error(errors);
                    }

                    this._logger.error(errors);
                    return throwError(errors);
                }),
                tap(fullResponse => {
                    try {
                        if (fullResponse instanceof HttpResponse && fullResponse.body && fullResponse.body as IResponseInterface) {
                            const response = fullResponse.body as IResponseInterface;
                            this._logger.debug('[ResponseInterceptor].intercept(...).IResponse: ', response);
                            if (this._translateService.currentLang) {
                                let messages = response.Messages ? response.Messages.filter(f => f.Language == this._translateService.currentLang) : [];
                                if(messages.length <= 0) {
                                    // if not found any message in setting lang - try get default english lang
                                    messages = response.Messages ? response.Messages.filter(f => f.Language == LanguagesEnum.English) : [];
                                }
                                Utils.Messages.ShowToastMessages(messages);
                            } else {
                                throwError('[ResponseInterceptor].intercept(...): Language is not set!');
                            }

                        } else {
                            throwError('[ResponseInterceptor].intercept(...): HttpResponse or IResponse CAST problem');
                        }
                    } catch (error) {
                        if (!error) {
                            error = 'Unhandled exception';
                        }
                        this._toaster.error(error);
                        this._logger.error(error);
                    }
                }));
    }
}
