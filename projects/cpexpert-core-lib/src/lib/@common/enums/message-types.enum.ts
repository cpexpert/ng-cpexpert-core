export enum MessageTypesEnum {
    Error = 1,
    Warning = 2,
    Information = 3,
    Debug = 4,
    Custom = 5
}
