export enum LocalStorageKeysEnum {
    FrontendEncryptedKey = 'FrontendEncryptedKey',
    CurrentAuthUser = 'CurrentAuthUser',
    RememberUser = 'RememberUser',
}
