export enum FuseNavigationItemTypesEnum {
    Item = 'item',
    Group = 'group',
    Collapsable = 'collapsable'
}
