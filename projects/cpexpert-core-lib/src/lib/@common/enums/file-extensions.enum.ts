export enum FileExtensionsEnum {
    // images
    Jpeg = 10,
    Jpg = 20,
    Bmp = 30,
    Png = 40,
    Gif = 50,

    // documents
    Docx = 100,
    Pdf = 110
}
