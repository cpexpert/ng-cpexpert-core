export enum SystemUserTypesEnum {
    Undefined = 0,
    SuperAdministrator = 1,
    Administrator = 2,
    User = 3,
    Tech = 4,
    Agent = 5
}
