export enum SystemOrganizationTypesEnum {
    Technical = 0,
    Customer = 1,
    Agency = 2
}
