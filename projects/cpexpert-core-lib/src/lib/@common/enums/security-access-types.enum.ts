export enum SecurityAccessTypesEnum {
    Read = 100,
    Write = 200,
    Remove = 300,
}
