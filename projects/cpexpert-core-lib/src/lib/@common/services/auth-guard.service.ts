import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Utils } from '../utils';
import { AuthService } from './http/auth.service';
import { AuthModule } from '../../@pages/public/auth/auth.module';
import { SystemUserTypesEnum } from '../enums/system-user-types.enum';

@Injectable()
export class AuthGuardService implements CanActivate {

  constructor(private _authService: AuthService, private _router: Router) {
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (!Utils.HasSessionExpired()) {
      const currentUserSystemRole = Utils.GetCurrentUserType();
      if (currentUserSystemRole !== SystemUserTypesEnum.Undefined) {
        if (currentUserSystemRole === SystemUserTypesEnum.SuperAdministrator) {
          return true;
        }
        const allowedSystemRoles = next.data.allowedSystemRoles as SystemUserTypesEnum[];
        return allowedSystemRoles.find(f => f === currentUserSystemRole) !== null;
      }
    }

    this._authService.logout();
    this._router.navigate([AuthModule.NavigationLinks.LoginPage]);
    return false;
  }
}
