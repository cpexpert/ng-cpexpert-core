import { Injectable, Output, EventEmitter, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { IResponseInterface } from '../../interfaces/i-response.interface';
import { HttpClient } from '@angular/common/http';
import { Utils } from '../../utils';
import { LocalStorageKeysEnum } from '../../enums/local-storage-keys.enum';
import { ENVIRONMENT_SETTINGS } from '../../../injection-tokens';
import { IRequestInterface } from '../../interfaces/i-request.interface';
import { TranslateService } from '@ngx-translate/core';
import { ProfileService } from './profile.service';
import { AuthResponseModel } from '../../models/auth-response.model';
import { AuthRequestModel } from '../../models/auth-request.model';

@Injectable({
  providedIn: 'root',
})
export class AuthService {

  constructor(
    private _httpClient: HttpClient,
    private _translateService: TranslateService,
    @Inject(ENVIRONMENT_SETTINGS) private environmentSettings: any
  ) {}

  @Output() loginSuccessEvent: EventEmitter<AuthResponseModel> = new EventEmitter<AuthResponseModel>();
  @Output() logoutSuccessEvent: EventEmitter<any> = new EventEmitter<any>();

  public login(requestData: AuthRequestModel, rememberUser: boolean): void {
    const request: IRequestInterface = {
      CanCache: false,
      Created: new Date(),
      Id: Utils.Guid.New(),
      RequestData: requestData,
      Language: Number(this._translateService.currentLang)
    };
    this._httpClient.post<IResponseInterface>(`${this.environmentSettings.HttpApiServices.Core}/auth/login`, request)
      .subscribe((response: IResponseInterface) => {
        if (response && response.ResponseData && response.IsSuccess) {
          const authUserData = response.ResponseData as AuthResponseModel;
          if (authUserData) {
            Utils.LocalStorage.Set(LocalStorageKeysEnum.CurrentAuthUser, authUserData);
            Utils.LocalStorage.Set(LocalStorageKeysEnum.RememberUser, rememberUser);
            this.loginSuccessEvent.emit(authUserData);
            return;
          }
        }

        this.clearLocalStorageUserData();
      }, () => { this.clearLocalStorageUserData(); });
  }

  public tryAutoLogin(): void {
    const autoLoginEnabled = Utils.LocalStorage.Get(LocalStorageKeysEnum.RememberUser);
    if (autoLoginEnabled) {
      const currentAuthUser = Utils.LocalStorage.Get(LocalStorageKeysEnum.CurrentAuthUser) as AuthResponseModel;
      if (currentAuthUser) {
        if(Utils.HasSessionExpired()) {
          // extend user auth data from server - extend time
          this._httpClient.get<IResponseInterface>(`${this.environmentSettings.HttpApiServices.Core}/auth/extendAuthData?encryptOldBarerToken=`+ Utils.Encrypt(currentAuthUser.BarerToken))
          .subscribe((response: IResponseInterface) => {
            if (response && response.ResponseData && response.IsSuccess) {
              const authUserData = response.ResponseData as AuthResponseModel;
              if (authUserData) {
                Utils.LocalStorage.Set(LocalStorageKeysEnum.CurrentAuthUser, authUserData);
                this.loginSuccessEvent.emit(authUserData);
                return;
              }
            }
    
            this.clearLocalStorageUserData();
          }, () => { this.clearLocalStorageUserData(); });
        } else {
          this.loginSuccessEvent.emit(currentAuthUser);
        }
      }
    } else if(!Utils.HasSessionExpired()) {
      const currentAuthUser = Utils.LocalStorage.Get(LocalStorageKeysEnum.CurrentAuthUser) as AuthResponseModel;
      if (currentAuthUser) {
        this.loginSuccessEvent.emit(currentAuthUser);
      }
    } else {
      this.logout();
    }
  }

  public logout(): void {   
    const userAppKey = Utils.GetCurrentUserAppKey();
    const request: IRequestInterface = {
      CanCache: false,
      Created: new Date(),
      Id: Utils.Guid.New(),
      RequestData: userAppKey !== null ? userAppKey : Utils.Guid.Empty,
      Language: Number(this._translateService.currentLang)
    };
    this.clearLocalStorageUserData();
    this._httpClient.post<IResponseInterface>(`${this.environmentSettings.HttpApiServices.Core}/auth/logout`, request).subscribe();
    this.logoutSuccessEvent.emit(userAppKey);
  }

  public register(requestData: any): Observable<IResponseInterface> {
    const request: IRequestInterface = {
      CanCache: false,
      Created: new Date(),
      Id: Utils.Guid.New(),
      RequestData: requestData,
      Language: Number(this._translateService.currentLang)
    };
    return this._httpClient.post<IResponseInterface>(`${this.environmentSettings.HttpApiServices.Core}/auth/register`, request);
  }

  private clearLocalStorageUserData(): void {
    Utils.LocalStorage.Remove(LocalStorageKeysEnum.CurrentAuthUser);
    Utils.LocalStorage.Remove(LocalStorageKeysEnum.RememberUser);
  }
}
