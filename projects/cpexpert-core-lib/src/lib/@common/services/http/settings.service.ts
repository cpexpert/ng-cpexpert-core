import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { ENVIRONMENT_SETTINGS } from '../../../injection-tokens';
import { IResponseInterface } from '../../interfaces/i-response.interface';
import { SettingKeysEnum } from '../../enums/setting-keys.enum';
import { IRequestInterface } from '../../interfaces/i-request.interface';
import { Utils } from '../../utils';

@Injectable({
    providedIn: 'root',
})
export class SettingService {

    constructor(
        private _httpClient: HttpClient,
        private _translateService: TranslateService,
        @Inject(ENVIRONMENT_SETTINGS) private environmentSettings: any
    ) {
    }

    public getFrontendEncryptedKey(): Observable<IResponseInterface> {
        return this._httpClient.get<IResponseInterface>(`${this.environmentSettings.HttpApiServices.Core}/settings/getFrontendEncryptedKey`);
    }

    public getSetting(key: SettingKeysEnum): Observable<IResponseInterface> {
        return this._httpClient.get<IResponseInterface>(`${this.environmentSettings.HttpApiServices.Core}/settings/getSetting?settingKey=${key}`);
    }

    public create(requestData: any): Observable<IResponseInterface> {
        const request: IRequestInterface = {
            CanCache: false,
            Created: new Date(),
            Id: Utils.Guid.New(),
            RequestData: requestData,
            Language: Number(this._translateService.currentLang)
        };
        return this._httpClient.post<IResponseInterface>(`${this.environmentSettings.HttpApiServices.Core}/settings`, request);
    }

    public update(requestData: any): Observable<IResponseInterface> {
        const request: IRequestInterface = {
            CanCache: false,
            Created: new Date(),
            Id: Utils.Guid.New(),
            RequestData: requestData,
            Language: Number(this._translateService.currentLang)
        };
        return this._httpClient.put<IResponseInterface>(`${this.environmentSettings.HttpApiServices.Core}/settings`, request);
    }

    public delete(settingAppKey: string): Observable<IResponseInterface> {
        return this._httpClient.delete<IResponseInterface>(`${this.environmentSettings.HttpApiServices.Core}/settings/${settingAppKey}`);
    }
}
