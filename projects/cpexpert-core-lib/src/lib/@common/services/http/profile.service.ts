import { Injectable, Inject, Output, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { NGXLogger } from 'ngx-logger';
import { TranslateService } from '@ngx-translate/core';
import { ENVIRONMENT_SETTINGS } from '../../../injection-tokens';
import { UserProfileModel } from '../../models/user-profile.model';
import { IResponseInterface } from '../../interfaces/i-response.interface';
import { Utils } from '../../utils';
import { IRequestInterface } from '../../interfaces/i-request.interface';

@Injectable({
    providedIn: 'root',
})
export class ProfileService {

    constructor(
        private _httpClient: HttpClient,
        private _logger: NGXLogger,
        private _translateService: TranslateService,
        @Inject(ENVIRONMENT_SETTINGS) private environmentSettings: any
    ) {}

    @Output() reloadProfileDataEvent: EventEmitter<UserProfileModel> = new EventEmitter<UserProfileModel>();

    public reloadProfileData(userAppKey: string): void {
        if (userAppKey === null) { return; }
        this._httpClient.get<IResponseInterface>(`${this.environmentSettings.HttpApiServices.Core}/profiles/getProfileData?userAppKey=${userAppKey}`)
        .subscribe((response: IResponseInterface) => {
            if (response && response.ResponseData && response.IsSuccess) {
              const profileResponseData = response.ResponseData as UserProfileModel;
              if (profileResponseData) {
                if (!profileResponseData.AvatarWebSrc)
                {
                    profileResponseData.AvatarWebSrc = Utils.LocalStorage.Constants.DefaultAvatarUrl;
                } 
                this.reloadProfileDataEvent.emit(profileResponseData);
                this._logger.debug('[ProfileService].reloadProfileData');
                return;
              }
            }

          }, () => { });
    }

    public setProfileData(requestData: any): Observable<IResponseInterface> {
        const request: IRequestInterface = {
            CanCache: false,
            Created: new Date(),
            Id: Utils.Guid.New(),
            RequestData: requestData,
            Language: Number(this._translateService.currentLang)
        };
        return this._httpClient.post<IResponseInterface>(`${this.environmentSettings.HttpApiServices.Core}/profiles/getProfileData`, request);
    }

    public uploadAvatar(avatar: Blob): Observable<IResponseInterface> {
        const formData = new FormData();
        formData.append('avatar', avatar);
        return this._httpClient.post<IResponseInterface>(`${this.environmentSettings.HttpApiServices.Core}/profiles/uploadAvatar?userAppKey=${Utils.GetCurrentUserAppKey()}`, formData);
    }

    public getAllProfilesForOrganization(organizationAppKey): Observable<IResponseInterface> {
        if (organizationAppKey === null) { return; }
        return this._httpClient.get<IResponseInterface>(`${this.environmentSettings.HttpApiServices.Core}/profiles/getAllProfilesForOrganization?organizationAppKey=${organizationAppKey}`);
    }
}
