import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NGXLogger } from 'ngx-logger';
import { Observable } from 'rxjs';
import { ENVIRONMENT_SETTINGS } from '../../../injection-tokens';
import { IResponseInterface } from '../../interfaces/i-response.interface';

@Injectable({
    providedIn: 'root',
})
export class UserService {

    constructor(
        private _httpClient: HttpClient,
        private _logger: NGXLogger,
        @Inject(ENVIRONMENT_SETTINGS) private environmentSettings: any
    ) {}

    public getAllOrganizationUsers(organizationAppKey: string): Observable<IResponseInterface> {
        return this._httpClient.get<IResponseInterface>(`${this.environmentSettings.HttpApiServices.Core}/users/getAllOrganizationUsers?organizationAppKey=${organizationAppKey}`);
    }
}
