import { Injectable, Inject } from '@angular/core';
import * as signalR from '@microsoft/signalr';
import { NGXLogger } from 'ngx-logger';
import { HttpClient } from '@angular/common/http';
import { ProfileService } from './profile.service';
import { ENVIRONMENT_SETTINGS } from '../../../injection-tokens';
import { IResponseInterface } from '../../interfaces/i-response.interface';
import { Utils } from '../../utils';

@Injectable({
    providedIn: 'root'
})
export class SignalRService {

    constructor(
        private _logger: NGXLogger,
        private _httpClient: HttpClient,
        private _profileService: ProfileService,
        @Inject(ENVIRONMENT_SETTINGS) private environmentSettings: any) {
    }

    private hubConnections: signalR.HubConnection[] = [];

    public startConnection = () => {
        this._httpClient.get(`${this.environmentSettings.HttpApiServices.SignalR}/hub/GetDefinedHubsUrls`)
            .subscribe((response: IResponseInterface) => {
                if (response && response.ResponseData && Array.isArray(response.ResponseData) && response.ResponseData.length > 0) {     
                    response.ResponseData.forEach((hubUrl: string) => {
                        const hubConnectionBuilder = new signalR.HubConnectionBuilder();
                        const hubConnection = hubConnectionBuilder.withUrl(hubUrl).withAutomaticReconnect().build();
                        hubConnection
                        .start()
                        .then(() => {
                            this._logger.debug(`[SignalR] Connection ${hubUrl} started success`);
                            this.sendToken(hubConnection);
                            if (hubUrl.includes('/hubs/command')) {
                                this.addCommandListener(hubConnection);
                            } else if (hubUrl.includes('/hubs/communication')) {
                                this.addCommunicationListener(hubConnection);
                            } else if (hubUrl.includes('/hubs/systemremotemethods')) {
                                this.addSystemRemoteMethodsListner(hubConnection);
                            }

                            this.hubConnections.push(hubConnection);
                        })
                        .catch(err => this._logger.error('[SignalR] Error while starting connection: ' + err));
                    });
                } else {
                    this._logger.error('[SignalR] No defined hubs!');
                }
            });
    }

    public reconnect(): void  {
        this.destroy();
        this.startConnection();
    }

    public sendToken(commandHubConnection: signalR.HubConnection): void {
        // commandHubConnection.invoke('AddUserId', Utils.GetCurrentUserToken());
    }

    private addSystemRemoteMethodsListner(commandHubConnection: signalR.HubConnection): void {
        commandHubConnection.on('Hub.SystemRemoteMethods.RefreshUserProfile', (data: string) => {
            this._logger.debug('[SignalR].Hub.SystemRemoteMethods.RefreshUserProfile: ', data);
            this._profileService.reloadProfileData(data);
        });

        this._logger.debug('[SignalR].Hub.SystemRemoteMethods CONNECT');
    }

    private addCommandListener(commandHubConnection: signalR.HubConnection): void {
        commandHubConnection.on('Hub.Command.ExecuteSuccess', (data) => {
            this._logger.debug('[SignalR].Hub.Command.ExecuteSuccess: ', data);
            Utils.Messages.ShowToastMessageToUser(data);
        });

        commandHubConnection.on('Hub.Command.ExecuteValidationFailure', (data) => {
            this._logger.debug('[SignalR].Hub.Command.ExecuteValidationFailure: ', data);
            Utils.Messages.ShowToastMessageToUser(data);
        });

        commandHubConnection.on('Hub.Command.Test', (data) => {
            this._logger.debug('[SignalR].Hub.Command.Test: ', data);
        });

        this._logger.debug('[SignalR].Hub.Command CONNECT');
    }
    private addCommunicationListener(communicationHubConnection: signalR.HubConnection): void {
        communicationHubConnection.on('Hub.Communication.ShowMessageToUsers', (data) => {
            this._logger.debug('[SignalR].Hub.Communication.ShowMessageToUsers: ', data);
        });
        communicationHubConnection.on('Hub.Communication.Test', (data) => {
            this._logger.debug('[SignalR].Hub.Communication.Test: ', data);
        });

        this._logger.debug('[SignalR].Hub.Communication CONNECT');
    }

    public destroy(): void {
        try {
            this.hubConnections.forEach(hubConnection => { hubConnection.stop(); });
        } catch (error) {
            this._logger.error(error);
        }
    }
}
