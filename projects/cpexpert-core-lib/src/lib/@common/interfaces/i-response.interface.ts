import { MessageModel } from '../models';

export interface IResponseInterface {
    Id: string;
    Created: Date;
    RequestId: string;
    RequestCreated: Date;
    Messages: MessageModel[];
    IsSuccess: boolean;
    ResponseData: any;
    CanCache: boolean;
}
