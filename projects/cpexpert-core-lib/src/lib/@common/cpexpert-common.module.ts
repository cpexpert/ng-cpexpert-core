import { NgModule, Injector } from '@angular/core';
import { LayoutModule } from './layout/layout.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { CacheInterceptor } from './interceptors/cache.interceptor';
import { ResponseInterceptor } from './interceptors/response.interceptor';
import { AuthGuardService } from './services/auth-guard.service';
import { RequestInterceptor } from './interceptors/request.interceptor';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { Utils } from './utils';
import { LoaderInterceptor } from './interceptors/loader.interceptor';
import { LoaderService } from './services/loader.service';
import { LoaderComponent } from './components/loader/loader.component';
import { ComponentsModule } from './components/components.module';

@NgModule({
    declarations: [],
    imports: [MatProgressSpinnerModule],
    exports: [
        LayoutModule,
        ComponentsModule
    ],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: ResponseInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: RequestInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: CacheInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true },
        AuthGuardService,
        LoaderService
    ]
})
export class CpExpertCommonModule {
    constructor(public injector: Injector) {
        Utils.AngularInjector = injector;
    }
}
