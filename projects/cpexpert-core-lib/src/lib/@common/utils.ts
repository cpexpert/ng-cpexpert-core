import { LocalStorageKeysEnum } from './enums/local-storage-keys.enum';
import { SystemUserTypesEnum } from './enums/system-user-types.enum';
import { LanguagesEnum } from './enums/languages.enum';
import * as CryptoJS from 'crypto-js';
import { JwtHelperService } from '@auth0/angular-jwt';
import { MessageTypesEnum } from './enums/message-types.enum';
import { throwError } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { NGXLogger } from 'ngx-logger';
import { Injector } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { CommandMessagesToUserModel } from './models/command-messages-to-user.model';
import { MessageModel } from './models/message.model';
import { AuthResponseModel } from './models/auth-response.model';
import { JwtModel } from './models/jwt.model';
import { RoleModel } from './models/role.model';

export class Utils {
   
    static AngularInjector: Injector = null;
    public static Languages = [
        {
            id: LanguagesEnum.English,
            title: 'English',
            flag: 'us',
            code: 'EN'
        },
        {
            id: LanguagesEnum.Polish,
            title: 'Polski',
            flag: 'pl',
            code: 'PL'
        }
    ];

    public static Guid = class {
        public static Empty(): string {
            return '00000000-0000-0000-0000-000000000000';
        }
        public static New(): string {
            let output = '';
            const value = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx';

            // tslint:disable-next-line: prefer-for-of
            for (let index = 0; index < value.length; index++) {
                const c = value[index];

                if (c === '-' || c === '4') {
                    output += c;
                    continue;
                }

                // tslint:disable-next-line:no-bitwise
                const r = (Math.random() * 16) | 0;
                // tslint:disable-next-line:no-bitwise
                const v = c === 'x' ? r : (r & 0x3) | 0x8;
                output += v.toString(16);
            }

            return output;
        }
    };

    public static UserChatStatuses = [
        {
            title: 'Online',
            icon: 'icon-checkbox-marked-circle',
            color: '#4CAF50'
        },
        {
            title: 'Away',
            icon: 'icon-clock',
            color: '#FFC107'
        },
        {
            title: 'Do not Disturb',
            icon: 'icon-minus-circle',
            color: '#F44336'
        },
        {
            title: 'Invisible',
            icon: 'icon-checkbox-blank-circle-outline',
            color: '#BDBDBD'
        },
        {
            title: 'Offline',
            icon: 'icon-checkbox-blank-circle-outline',
            color: '#616161'
        }
    ];

    public static LocalStorage = class {

        public static Constants = class {
            public static DefaultAvatarUrl = 'assets/images/avatars/profile.jpg';
        };

        public static Set(key: LocalStorageKeysEnum, data: any): boolean {
            localStorage.removeItem(key);
            if (data) {
                const encryptedData = CryptoJS.enc.Base64.stringify(CryptoJS.enc.Utf8.parse(JSON.stringify(data)));
                localStorage.setItem(key, encryptedData);
                return localStorage.getItem(key) != null;
            }
            return false;
        }

        public static Get(key: LocalStorageKeysEnum): any {
            const data = localStorage.getItem(key);
            if (data) {
                return JSON.parse(CryptoJS.enc.Utf8.stringify(CryptoJS.enc.Base64.parse(data)));
            }
            return null;
        }

        public static Remove(key: LocalStorageKeysEnum): boolean {
            localStorage.removeItem(key);
            return localStorage.getItem(key) == null;
        }
    };

    public static Messages = class {
        
        public static ShowToastMessageToUser(data: CommandMessagesToUserModel, ): void {
            if (data && data.Messages && data.UserAppKey && data.UserAppKey !== Utils.Guid.Empty()) {
                if (data.UserAppKey === Utils.GetCurrentUserAppKey()) {
                    this.ShowToastMessages(data.Messages);
                }
            }
        }
    
        public static ShowToastMessages(messages: MessageModel[]): void {
            if (messages && messages.length > 0 && Utils.AngularInjector) {
                const toaster = Utils.AngularInjector.get(ToastrService);
                const logger = Utils.AngularInjector.get(NGXLogger);
                if (toaster && logger) {
                    messages.forEach((message: MessageModel) => {
                        switch (message.Type) {
                            case MessageTypesEnum.Custom:
                                toaster.info(message.Message);
                                break;
                            case MessageTypesEnum.Information:
                                toaster.info(message.Message);
                                break;
                            case MessageTypesEnum.Warning:
                                toaster.warning(message.Message);
                                break;
                            case MessageTypesEnum.Debug:
                                logger.debug(message.Message);
                                break;
                            case MessageTypesEnum.Error:
                                logger.error(message.Message);
                                break;
                            default:
                                throwError('[ResponseInterceptor].intercept(...): Unhandled exception when show IResponse.Messages');
                        }
                    });
                }
            }
        }
    };

    public static GetCurrentUserToken(): string {
        const currentUserCreditionals = Utils.LocalStorage.Get(LocalStorageKeysEnum.CurrentAuthUser) as AuthResponseModel;
        if (currentUserCreditionals && currentUserCreditionals.BarerToken) {
            return currentUserCreditionals.BarerToken;
        }

        return null;
    }

    public static HasSessionExpired(): boolean {
        const currentUserCreditionals = Utils.LocalStorage.Get(LocalStorageKeysEnum.CurrentAuthUser) as AuthResponseModel;
        if (currentUserCreditionals && currentUserCreditionals.UserAppKey && currentUserCreditionals.UserEmail && currentUserCreditionals.BarerToken) {
            const helper = new JwtHelperService();
            if (!helper.isTokenExpired(currentUserCreditionals.BarerToken)) {
                console.log('User: ' + currentUserCreditionals.UserEmail + ' session remaining time: ' + helper.getTokenExpirationDate(currentUserCreditionals.BarerToken));
                return false;
            }
        }

        return true;
    }

    public static GetCurrentUserType(): SystemUserTypesEnum {
        try {
            if (!Utils.HasSessionExpired()) {
                const currentUserCreditionals = Utils.LocalStorage.Get(LocalStorageKeysEnum.CurrentAuthUser) as AuthResponseModel;
                if (currentUserCreditionals && currentUserCreditionals.UserAppKey && currentUserCreditionals.UserEmail && currentUserCreditionals.BarerToken) {
                    const helper = new JwtHelperService();
                    const payloadData = helper.decodeToken(currentUserCreditionals.BarerToken) as JwtModel;
                    return payloadData.UserType;
                }
            }
        } catch (error) {
            console.error(error);
            return SystemUserTypesEnum.Undefined;
        }

        return SystemUserTypesEnum.Undefined;
    }

    public static GetCurrentUserOrganizationAppKey(): string {
        try {
            if (!Utils.HasSessionExpired()) {
                const currentUserCreditionals = Utils.LocalStorage.Get(LocalStorageKeysEnum.CurrentAuthUser) as AuthResponseModel;
                if (currentUserCreditionals && currentUserCreditionals.UserAppKey && currentUserCreditionals.UserEmail && currentUserCreditionals.BarerToken) {
                    const helper = new JwtHelperService();
                    const payloadData = helper.decodeToken(currentUserCreditionals.BarerToken) as JwtModel;
                    return payloadData.OrganizationAppKey;
                }
            }
        } catch (error) {
            console.error(error);
            return null;
        }

        return null;
    }

    public static GetCurrentUserSystemRoles(): RoleModel[] {
        try {
            if (!Utils.HasSessionExpired()) {
                const currentUserCreditionals = Utils.LocalStorage.Get(LocalStorageKeysEnum.CurrentAuthUser) as AuthResponseModel;
                if (currentUserCreditionals && currentUserCreditionals.UserAppKey && currentUserCreditionals.UserEmail && currentUserCreditionals.BarerToken) {
                    const helper = new JwtHelperService();
                    const payloadData = helper.decodeToken(currentUserCreditionals.BarerToken) as JwtModel;
                    return payloadData.Roles;
                }
            }
        } catch (error) {
            console.error(error);
            return [];
        }

        return [];
    }

    static GetCurrentUserAppKey(): string {
        try {
            const currentUserCreditionals = Utils.LocalStorage.Get(LocalStorageKeysEnum.CurrentAuthUser) as AuthResponseModel;
            if (currentUserCreditionals && currentUserCreditionals.UserAppKey && currentUserCreditionals.UserEmail && currentUserCreditionals.BarerToken) {
                return currentUserCreditionals.UserAppKey;
            }
        } catch (error) {
            console.error(error);
            return null;
        }

        return null;
    }

    public static Encrypt(dataToEncrypt: string): string {
        const keyHex = CryptoJS.enc.Utf8.parse(Utils.LocalStorage.Get(LocalStorageKeysEnum.FrontendEncryptedKey));
        return  CryptoJS.TripleDES.encrypt(dataToEncrypt, keyHex, {
            iv: keyHex,
            mode: CryptoJS.mode.ECB,
            padding: CryptoJS.pad.Pkcs7
        }).toString();
    }

    public static Decrypt(encryptedData: string): string {
        const keyHex = CryptoJS.enc.Utf8.parse(Utils.LocalStorage.Get(LocalStorageKeysEnum.FrontendEncryptedKey));
        return CryptoJS.TripleDES.decrypt(encryptedData, keyHex, {
            iv: keyHex,
            mode: CryptoJS.mode.ECB,
            padding: CryptoJS.pad.Pkcs7
        }).toString(CryptoJS.enc.Utf8);
    }

    public static GetCurrentLocaleCode(): string {
        const translateService = Utils.AngularInjector.get(TranslateService);
        if(translateService) {
            switch(translateService.currentLang) {
                case LanguagesEnum.English:
                    return 'en-US';
                case LanguagesEnum.Polish:
                    return 'pl-PL';
            }
        }
    }
}
