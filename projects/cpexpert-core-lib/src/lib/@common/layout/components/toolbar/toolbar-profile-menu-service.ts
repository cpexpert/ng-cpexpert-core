import { ToolbarProfileMenuElement } from './toolbar-profile-menu-element';
import { Injectable } from '@angular/core';

@Injectable()
export class ToolbarProfileMenuService {

    private toolbarProfileMenuItems: ToolbarProfileMenuElement[] = [];

    constructor() {
    }

    public GetToolbarProfileMenuItems(): ToolbarProfileMenuElement[] {
        return this.toolbarProfileMenuItems;
    }

    public SetToolbarProfileMenuItems(element: ToolbarProfileMenuElement): void {
        this.toolbarProfileMenuItems.push(element);
    }
}
