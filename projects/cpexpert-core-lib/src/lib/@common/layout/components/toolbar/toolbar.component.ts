import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import * as _ from 'lodash-es';
import { FuseConfigService } from '../../../../@fuse/services/config.service';
import { FuseSidebarService } from '../../../../@fuse/components/sidebar/sidebar.service';
import { FuseNavigationService } from '../../../../@fuse/components/navigation/navigation.service';
import { NGXLogger } from 'ngx-logger';
import { locale as english } from './i18n/en';
import { locale as polish } from './i18n/pl';
import { FuseTranslationLoaderService } from '../../../../@fuse/services/translation-loader.service';
import { Utils } from '../../../../@common/utils';
import { AuthService, ProfileService } from '../../../services';
import { ToolbarProfileMenuElement } from './toolbar-profile-menu-element';
import { ProfileModule } from '../../../../@pages/private/profile/profile.module';
import { Router } from '@angular/router';
import { UserProfileModel } from '../../../models/user-profile.model';

@Component({
    selector: 'toolbar',
    templateUrl: './toolbar.component.html',
    styleUrls: ['./toolbar.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class ToolbarComponent implements OnInit, OnDestroy {
    horizontalNavbar: boolean;
    rightNavbar: boolean;
    hiddenNavbar: boolean;
    navigation: any;
    selectedLanguage: any;
    languages: any[];
    userStatusOptions: any[];
    profileMenuElements: ToolbarProfileMenuElement[] = [];

    private _unsubscribeAll: Subject<any>;

    public openMailBoxClickHandler: any;

    public _userProfileData: UserProfileModel = {
        AppKey: '',
        AvatarWebSrc: Utils.LocalStorage.Constants.DefaultAvatarUrl,
        EMail: '',
        FirstName: '',
        LastName: '',
        PhoneNumber: '',
        UserAppKey: '',
        FullName: ''
    };

    constructor(
        private _fuseConfigService: FuseConfigService,
        private _fuseSidebarService: FuseSidebarService,
        private _translateService: TranslateService,
        private _logger: NGXLogger,
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private _authService: AuthService,
        private _profileService: ProfileService,
        private _fuseNavigationService: FuseNavigationService,
        private _router: Router
    ) {
        this.userStatusOptions = Utils.UserChatStatuses;
        this._fuseTranslationLoaderService.loadTranslations(english, polish);
        this.navigation = this.navigation || this._fuseNavigationService.getCurrentNavigation();
        this._unsubscribeAll = new Subject();
        this.languages = Utils.Languages;
    }

    ngOnInit(): void {
        this._profileService.reloadProfileData(Utils.GetCurrentUserAppKey());
        this._profileService.reloadProfileDataEvent.subscribe((userProfileData: UserProfileModel) => { this._userProfileData = userProfileData; });
        this._translateService.onLangChange.pipe(takeUntil(this._unsubscribeAll)).subscribe(() => { this.reloadToolbarMenu(); })
        this._fuseConfigService.config.pipe(takeUntil(this._unsubscribeAll)).subscribe((settings) => {
            this.horizontalNavbar = settings.layout.navbar.position === 'top';
            this.rightNavbar = settings.layout.navbar.position === 'right';
            this.hiddenNavbar = settings.layout.navbar.hidden === true;
        });
        this.reloadToolbarMenu();
        this.selectedLanguage = this.languages.find(f => f.id === this._translateService.currentLang);
    }

    ngOnDestroy(): void {
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    toggleSidebarOpen(key): void {
        this._fuseSidebarService.getSidebar(key).toggleOpen();
    }

    search(value): void {
        this._logger.debug(value);
    }

    setLanguage(lang): void {
        this.selectedLanguage = lang;
        this._translateService.use(lang.id);
    }

    profileMenuClickHandler(event: ToolbarProfileMenuElement): void {
        if (event) { event.Action(); }
    }

    reloadToolbarMenu(): void {
        this.profileMenuElements = [];
        this._translateService.get('Toolbar.MyProfile').subscribe((text: string) => {
            this.profileMenuElements.push(new ToolbarProfileMenuElement('profile', 'account_circle', text, () => { this._router.navigateByUrl(ProfileModule.NavigationLinks.ProfilePage); }));
        });
        this._translateService.get('Toolbar.Logout').subscribe((text: string) => {
            this.profileMenuElements.push(new ToolbarProfileMenuElement('logout', 'exit_to_app', text, () => { this._authService.logout(); }));
        });
    }
}
