import { LanguagesEnum } from '../../../../enums/languages.enum';

export const locale = {
    lang: LanguagesEnum.English,
    data: {
        Toolbar: {
            MyProfile: 'Profile',
            Mailbox: 'Mailbox',
            Tasks: 'Tasks',
            Logout: 'Logout',
            Errors: {
                CantGetProfileData: 'Can\'t found profile'
            }
        },
    }
};
