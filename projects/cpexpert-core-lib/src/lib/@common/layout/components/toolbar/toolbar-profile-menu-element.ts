export class ToolbarProfileMenuElement {

    constructor(key: string, iconCode: string, label: string, action: any) {
        this.Key = key;
        this.IconCode = iconCode;
        this.Label = label;
        this.Action = action;
    }

    public Key: string;
    public IconCode: string;
    public Label: string;
    public Action: any;
}
