import { LanguagesEnum } from '../../../../enums/languages.enum';

export const locale = {
    lang: LanguagesEnum.Polish,
    data: {
        Toolbar: {
            MyProfile: 'Profil',
            Mailbox: 'Wiadomości',
            Tasks: 'Zadania',
            Logout: 'Wyloguj',
            Errors: {
                CantGetProfileData: 'Błąd podczas pobierania danych profilu'
            }
        },
    }
};
