import { SystemUserTypesEnum as SystemUserTypes } from '../enums/system-user-types.enum';
import { RoleModel } from './role.model';

export class JwtModel {
    UserAppKey: string;
    CreateDate: Date;
    ExpirationDate: Date;
    Roles: RoleModel[];
    UserType: SystemUserTypes;
    OrganizationAppKey?: string;
}
