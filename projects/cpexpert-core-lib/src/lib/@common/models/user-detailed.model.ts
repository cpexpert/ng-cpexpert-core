export class UserDetailedModel {
    AppKey: string;
    AvatarWebSrc: string;
    FirstName: string;
    LastName: string;
    PhoneNumber: string;
    EMail: string;
    ProfileAppKey: string;
    FullName: string;
}
