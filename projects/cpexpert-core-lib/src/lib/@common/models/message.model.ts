import { MessageTypesEnum } from '../enums/message-types.enum';
import { LanguagesEnum } from '../enums/languages.enum';

export class MessageModel {
    Type: MessageTypesEnum;
    Message: string;
    Language: LanguagesEnum;
}
