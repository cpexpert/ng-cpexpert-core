export class UserProfileModel {
    AppKey: string;
    AvatarWebSrc: string;
    FirstName: string;
    LastName: string;
    PhoneNumber: string;
    EMail: string;
    UserAppKey: string;
    FullName: string;
}
