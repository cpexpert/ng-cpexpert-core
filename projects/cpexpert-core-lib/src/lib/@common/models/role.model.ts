import { PermissionModel } from './permission.model';

export class RoleModel {
    RoleName: string;
    Permissions: PermissionModel[];
}