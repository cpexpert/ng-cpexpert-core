import { SecurityAccessTypesEnum } from '../enums/security-access-types.enum';

export class PermissionModel {
    PermissionName: string;
    ElementCode: string;
    ElementSecurityAccessSchemaList: SecurityAccessTypesEnum[];
}
