import { NgModule } from "@angular/core";
import { MatButtonModule } from "@angular/material/button";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatIconModule } from "@angular/material/icon";
import { MatMenuModule } from "@angular/material/menu";
import { MatSelectModule } from "@angular/material/select";
import { MatTabsModule } from "@angular/material/tabs";
import { ChartsModule } from "ng2-charts";
import { NgxChartsModule } from "@swimlane/ngx-charts";
import { AnalyticsDashboardComponent } from "./analytics.component";
import { FuseSharedModule } from "../../../../@fuse/shared.module";
import { FuseWidgetModule } from "../../../../@fuse/components/widget/widget.module";
import { CpExpertModule } from '../../../../@common/abstracts/cp-expert-module';

@NgModule({
    declarations: [AnalyticsDashboardComponent],
    imports: [
        MatButtonModule,
        MatFormFieldModule,
        MatIconModule,
        MatMenuModule,
        MatSelectModule,
        MatTabsModule,
        ChartsModule,
        NgxChartsModule,
        FuseSharedModule,
        FuseWidgetModule,
    ],
    providers: [],
})
export class AnalyticsDashboardModule extends CpExpertModule {
    public BasePathElement: ''
}
