import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatTabsModule } from '@angular/material/tabs';
import { ProfileComponent } from './profile.component';
import { RouterModule } from '@angular/router';
import { FuseSharedModule } from '../../../@fuse/shared.module';
import { AuthGuardService } from '../../../@common/services/auth-guard.service';
import { ProfileService } from '../../../@common/services/http/profile.service';
import { SystemUserTypesEnum } from '../../../@common/enums/system-user-types.enum';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
    declarations: [
        ProfileComponent
    ],
    imports: [
        RouterModule,
        MatButtonModule,
        MatDividerModule,
        MatIconModule,
        MatTabsModule,
        FuseSharedModule,
        TranslateModule.forChild(),
    ],
    providers: [
        ProfileService
    ]
})
export class ProfileModule {
    static BasePathElement = '';
    static NavigationLinks = {
        ProfilePage: '/' + ProfileComponent.Name,
    };
    static Routes = {
        path: ProfileComponent.Name,
        component: ProfileComponent,
        canActivate: [AuthGuardService],
        data: {
            allowedSystemRoles: [SystemUserTypesEnum.Administrator, SystemUserTypesEnum.User], 
        }
    };
}
