import { LanguagesEnum } from '../../../../@common';

export const locale = {
    lang: LanguagesEnum.Polish,
    data: {
        Profile: {
            Fallow: 'Fallow',
            SendMessage: 'Send message'
        }
    }
};
