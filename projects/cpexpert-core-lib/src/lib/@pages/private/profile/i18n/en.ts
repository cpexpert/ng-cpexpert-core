import { LanguagesEnum } from '../../../../@common';

export const locale = {
    lang: LanguagesEnum.English,
    data: {
        Profile: {
            Fallow: 'Fallow',
            SendMessage: 'Send message'
        }
    }
};
