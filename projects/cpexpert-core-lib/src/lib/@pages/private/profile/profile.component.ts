import { IResponseInterface } from '../../../@common/interfaces/i-response.interface';
import { Utils } from './../../../@common/utils';
import { fuseAnimations } from './../../../@fuse/animations/index';
import { Component, ViewEncapsulation, OnInit, OnDestroy } from '@angular/core';
import { NGXLogger } from 'ngx-logger';
import { ToastrService } from 'ngx-toastr';
import { UserProfileModel } from '../../../@common/models/user-profile.model';
import { ProfileService } from '../../../@common/services/http/profile.service';
import { locale as english } from './i18n/en';
import { locale as polish } from './i18n/pl';
import { FuseTranslationLoaderService } from '../../../@fuse/services/translation-loader.service';

@Component({
    selector: 'profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ProfileComponent implements OnInit, OnDestroy {
    public static readonly Name = 'profile';

    // Public
    public _userProfileData: UserProfileModel = {
        AppKey: '',
        AvatarWebSrc: Utils.LocalStorage.Constants.DefaultAvatarUrl,
        EMail: '',
        FirstName: '',
        LastName: '',
        PhoneNumber: '',
        UserAppKey: '',
        FullName: ''
    };

    constructor(
        private _profileService: ProfileService, 
        private _logger: NGXLogger, 
        private _toaster: ToastrService,
        private fuseTranslationLoaderService: FuseTranslationLoaderService) {
        this.fuseTranslationLoaderService.loadTranslations(english, polish);
    }
    ngOnInit(): void {
        this._profileService.reloadProfileDataEvent.subscribe((userProfileData: UserProfileModel) => { this._userProfileData = userProfileData; });
    }
    ngOnDestroy(): void {
        this._logger.debug('[ProfileComponent]: DESTROY');
    }

    uploadAvatar(event: any): void {
        if (event && event.target && event.target.files && event.target.files[0]) {
            const avatar: Blob = event.target.files[0];
            if (avatar.size === 0) {
                this._toaster.warning('Avatar file is empty or too small!');
            }
            const avatarKBSize = Math.round(avatar.size / 1024); 
            if (avatarKBSize <= 250) {
                this._profileService.uploadAvatar(avatar).subscribe((response: IResponseInterface) => {
                    if (response && response.IsSuccess) {
                        this._profileService.reloadProfileData(Utils.GetCurrentUserAppKey());
                    }
                });
            } else {
                this._toaster.warning('Avatar is too large! Max avatar size is: 250 kB, acctual size: ' + avatarKBSize + ' kB');
            }
        }
    }
}
