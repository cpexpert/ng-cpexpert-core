import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FuseSharedModule } from '../../../@fuse/shared.module';
import { AuthGuardService } from '../../../@common/services/auth-guard.service';
import { SystemUserTypesEnum } from '../../../@common/enums/system-user-types.enum';
import { ProfileService } from '../../../@common/services/http/profile.service';
import { UserListComponent } from './user-list/user-list.component';
import { FuseWidgetModule } from '../../../@fuse/components/widget/widget.module';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MatRippleModule } from '@angular/material/core';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
    declarations: [
        UserListComponent
    ],
    imports: [
        MatButtonModule,
        MatChipsModule,
        MatExpansionModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatPaginatorModule,
        MatRippleModule,
        MatSelectModule,
        MatSortModule,
        MatSnackBarModule,
        MatTableModule,
        MatTabsModule,
        NgxChartsModule,
        RouterModule,
        FuseSharedModule,
        FuseWidgetModule,
        TranslateModule.forChild(),
    ],
    exports: [
        UserListComponent
    ],
    providers: [
        ProfileService
    ]
})
export class UserModule {
    static BasePathElement = 'users';
    static NavigationLinks = {
        UserList: '/' + UserModule.BasePathElement + '/' + UserListComponent.Name,
    };
    static Routes = {
        path: UserModule.BasePathElement,
        children: [
            {
                path: UserListComponent.Name,
                component: UserListComponent,
                canActivate: [AuthGuardService],
                data: {
                    allowedSystemRoles: [
                        SystemUserTypesEnum.Administrator, 
                        SystemUserTypesEnum.User,
                        SystemUserTypesEnum.SuperAdministrator
                    ], 
                }
            }
        ]
    };
}
