import { LanguagesEnum } from '../../../../../@common';

export const locale = {
    lang: LanguagesEnum.Polish,
    data: {
        UserList: {
            Title: 'Użytkownicy organizacji',
            AddItem: 'Dodaj',
            Search: 'Szukaj użytkownika',
            Table: {
                Header: {
                    AppKey: 'AppKey'
                }
            }
        }
    }
};
