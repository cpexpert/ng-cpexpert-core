import { LanguagesEnum } from '../../../../../@common';

export const locale = {
    lang: LanguagesEnum.English,
    data: {
        UserList: {
            Title: 'Organization users',
            AddItem: 'Add',
            Search: 'Search',
            Table: {
                Header: {
                    AppKey: 'AppKey'
                }
            }
        }
    }
};
