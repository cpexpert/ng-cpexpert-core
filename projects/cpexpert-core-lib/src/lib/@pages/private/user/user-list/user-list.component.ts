import { Component, OnInit, ViewEncapsulation, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { locale as english } from './i18n/en';
import { locale as polish } from './i18n/pl';
import { Subject } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { NGXLogger } from 'ngx-logger';
import { fuseAnimations } from '../../../../@fuse/animations';
import { FuseTranslationLoaderService } from '../../../../@fuse/services/translation-loader.service';
import { Utils, UserService, IResponseInterface } from 'projects/cpexpert-core-lib/src/lib/@common';
import { UserDetailedModel } from '../../../../@common/models/user-detailed.model';

@Component({
    selector: 'user-list',
    templateUrl: './user-list.component.html',
    styleUrls: ['./user-list.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations    
})
export class UserListComponent implements OnInit, OnDestroy {

    public static readonly Name = 'user-list';
    public dataList: UserDetailedModel[] = []
    public displayedColumns = [
        'AppKey',
    ];

    @ViewChild(MatPaginator, {static: true})
    paginator: MatPaginator;

    @ViewChild(MatSort, {static: true})
    sort: MatSort;

    @ViewChild('filter', {static: true})
    filter: ElementRef;

    private unsubscribeAll: Subject<any>;

    constructor(
        private fuseTranslationLoaderService: FuseTranslationLoaderService,
        private route: ActivatedRoute,
        private logger: NGXLogger,
        private userService: UserService,
        private location: Location)
    {
        this.unsubscribeAll = new Subject();
        this.fuseTranslationLoaderService.loadTranslations(english, polish);
    }

    ngOnInit(): void {
        const organizationAppKey = Utils.GetCurrentUserOrganizationAppKey();
        if(organizationAppKey) {
            this.userService.getAllOrganizationUsers(organizationAppKey)
            .subscribe((response: IResponseInterface) => {
                if(response && response.IsSuccess) {
                    this.dataList = response.ResponseData; 
                }
            })
        }
    }

    ngOnDestroy(): void {
        this.unsubscribeAll.next();
        this.unsubscribeAll.complete();
    }

    goBack(event: any) {
        this.location.back();
    }
}