import { NgModule } from '@angular/core';
import { Route } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { ProfileModule } from './profile/profile.module';
import { UserModule } from './user/user.module';

@NgModule({
    declarations: [],
    imports: [
        TranslateModule.forChild()
    ],
    exports: [
        ProfileModule
    ],
    providers: []
})
export class PrivatePagesModule {
    static Routes: Route[] = [
        ProfileModule.Routes,
        UserModule.Routes
    ];
}
