import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { locale as english } from './i18n/en';
import { locale as polish } from './i18n/pl';
import { FuseTranslationLoaderService } from '../../../../@fuse/services/translation-loader.service';
import { FuseConfigService } from '../../../../@fuse/services/config.service';
import { Location } from '@angular/common';
import { ErrorsModule } from '../errors.module';
import { Router } from '@angular/router';
import { NGXLogger } from 'ngx-logger';

@Component({
    selector: 'error-500',
    templateUrl: './error-500.component.html',
    styleUrls: ['./error-500.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class Error500Component implements OnInit {
    public static readonly Name = 'error-500';
    public backButtonShow: boolean;
    public registerErrorButtonShow: boolean;
    public readonly ReportPagePath = ErrorsModule.NavigationLinks.ReportError;
    constructor(private _fuseConfigService: FuseConfigService,
                private _fuseTranslationLoaderService: FuseTranslationLoaderService,
                private _location: Location,
                private _router: Router,
                private _logger: NGXLogger) {

        this.backButtonShow = false;
        this.registerErrorButtonShow = false;
        const extras = this._router.getCurrentNavigation().extras;
        if (extras && extras.state) {
            if (extras.state.backButtonShow) {
                this.backButtonShow = extras.state.backButtonShow;
            }
            if (extras.state.registerErrorButtonShow) {
                this.registerErrorButtonShow = extras.state.registerErrorButtonShow;
            }
        }

        this._fuseConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer: {
                    hidden: false
                },
                sidepanel: {
                    hidden: true
                }
            }
        };
        
        this._fuseTranslationLoaderService.loadTranslations(english, polish);
    }

    ngOnInit(): void {

    }

    onClickHandler($event: any): void {
        this._location.back();
    }
}
