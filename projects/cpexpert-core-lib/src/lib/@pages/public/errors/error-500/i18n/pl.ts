import { LanguagesEnum } from '../../../../../@common/enums/languages.enum';

export const locale = {
    lang: LanguagesEnum.Polish,
    data: {
        Error500Page: {
            ErrorMessage1: 'Wystąpił wewnętrzny błąd systemu',
            ErrorMessage2: 'Może być to spowodowane chwilowym przerwaniem dostepu do usług, proszę odczekać kilka minut i spróbować ponownie.',
            ErrorMessage3: 'W przypadku ponownego wystąpienia problemu skorzystaj z opcji zgłoszenia błędu',     
            GoBack: 'Wróć',
            ReportThisProblem: 'Zgłoś problem'
        }
    }
};
