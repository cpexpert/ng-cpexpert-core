import { LanguagesEnum } from '../../../../../@common/enums/languages.enum';

export const locale = {
    lang: LanguagesEnum.English,
    data: {
        Error500Page: {
            ErrorMessage1: 'An internal system error has occurred',
            ErrorMessage2: 'This may be due to a temporary interruption of access to services, please wait a few minutes and try again.',
            ErrorMessage3: 'If the problem reoccurs, use the error reporting option',
            GoBack: 'Go back',
            ReportThisProblem: 'Report this problem'
        }
    }
};
