import { LanguagesEnum } from '../../../../../@common/enums/languages.enum';

export const locale = {
    lang: LanguagesEnum.English,
    data: {
        Error404Page: {
            Sorry: 'Sorry but we could not find the page you are looking for',
            SearchForSomething: 'Search for something',
            GoBack: 'Go back'
        }
    }
};
