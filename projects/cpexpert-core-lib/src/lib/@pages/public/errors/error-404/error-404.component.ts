import { Component, ViewEncapsulation } from '@angular/core';
import { locale as english } from './i18n/en';
import { locale as polish } from './i18n/pl';
import { FuseTranslationLoaderService } from '../../../../@fuse/services/translation-loader.service';
import { FuseConfigService } from '../../../../@fuse/services/config.service';
import { Location } from '@angular/common';
import { ErrorsModule } from '../errors.module';

@Component({
    selector: 'error-404',
    templateUrl: './error-404.component.html',
    styleUrls: ['./error-404.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class Error404Component {
    public static readonly Name = 'error-404';
    constructor(private _fuseConfigService: FuseConfigService,
                private _fuseTranslationLoaderService: FuseTranslationLoaderService,
                private _location: Location) {
        this._fuseConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer: {
                    hidden: false
                },
                sidepanel: {
                    hidden: true
                }
            }
        };
        
        this._fuseTranslationLoaderService.loadTranslations(english, polish);
    }

    onClickHandler($event: any): void {
        this._location.back();
    }
}
