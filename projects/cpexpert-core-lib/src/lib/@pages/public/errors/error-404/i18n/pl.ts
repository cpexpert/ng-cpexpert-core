import { LanguagesEnum } from '../../../../../@common/enums/languages.enum';

export const locale = {
    lang: LanguagesEnum.Polish,
    data: {
        Error404Page: {
            Sorry: 'Przepraszamy, ale nie byliśmy w stanie odnaleźć w naszym systemie szukanej przez Ciebie strony',
            SearchForSomething: 'Szukaj',
            GoBack: 'Wróć'
        }
    }
};
