import { NgModule } from '@angular/core';
import { Error404Component } from './error-404/error-404.component';
import { Error500Component } from './error-500/error-500.component';
import { MatIconModule } from '@angular/material/icon';
import { FuseSharedModule } from '../../../@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule, Routes, Route } from '@angular/router';
import { ErrorReportComponent } from './error-report/error-report.component';

@NgModule({
    declarations: [
        Error404Component,
        Error500Component,
        ErrorReportComponent
    ],
    imports: [
        RouterModule,
        TranslateModule.forChild(),
        MatIconModule,
        FuseSharedModule
    ],
    exports: [
        Error404Component,
        Error500Component,
        ErrorReportComponent
    ],
    providers: [
    ]
})
export class ErrorsModule {
    static BasePathElement = 'errors';
    static NavigationLinks = {
        Error404Page: '/' + ErrorsModule.BasePathElement + '/' + Error404Component.Name,
        Error500Page: '/' + ErrorsModule.BasePathElement + '/' + Error500Component.Name,
        ReportError: '/' + ErrorsModule.BasePathElement + '/' + ErrorReportComponent.Name,
    };
    static Routes: Route = {
        path: ErrorsModule.BasePathElement,
        children: [
            {
                path: Error404Component.Name,
                component: Error404Component
            },
            {
                path: Error500Component.Name,
                component: Error500Component
            },
            {
                path: ErrorReportComponent.Name,
                component: ErrorReportComponent
            }
        ]
    };
}
