import { Component, ViewEncapsulation } from '@angular/core';
import { locale as english } from './i18n/en';
import { locale as polish } from './i18n/pl';
import { FuseTranslationLoaderService } from '../../../../@fuse/services/translation-loader.service';
import { FuseConfigService } from '../../../../@fuse/services/config.service';

@Component({
    selector: 'error-report',
    templateUrl: './error-report.component.html',
    styleUrls: ['./error-report.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ErrorReportComponent {
    public static readonly Name = 'error-report';
    constructor(private _fuseConfigService: FuseConfigService,
                private _fuseTranslationLoaderService: FuseTranslationLoaderService) {
        this._fuseConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer: {
                    hidden: false
                },
                sidepanel: {
                    hidden: true
                }
            }
        };
        
        this._fuseTranslationLoaderService.loadTranslations(english, polish);
    }
}
