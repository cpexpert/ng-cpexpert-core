import { NgModule } from '@angular/core';
import { SearchClassicComponent } from './classic/search-classic.component';
import { SearchModernComponent } from './modern/search-modern.component';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { SearchClassicService } from './classic/search-classic.service';
import { SearchModernService } from './modern/search-modern.service';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { FuseSharedModule } from '../../../@fuse/shared.module';

@NgModule({
    declarations: [
        SearchClassicComponent,
        SearchModernComponent,
    ],
    imports: [
        RouterModule,
        MatButtonModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatTableModule,
        MatTabsModule,
        FuseSharedModule,
        TranslateModule.forChild()
    ],
    exports: [
        SearchClassicComponent,
        SearchModernComponent
    ],
    providers: [
        SearchClassicService,
        SearchModernService
    ]
})
export class SearchModule {
    static BasePathElement = 'search';
    static NavigationLinks = {
        SearchClassicPage: SearchModule.BasePathElement + '/' + SearchClassicComponent.Name,
        SearchModernPage: SearchModule.BasePathElement + '/' + SearchModernComponent.Name,
    };
    static Routes =
    {
        path: SearchModule.BasePathElement,
        children: [
            {
                path: SearchClassicComponent.Name,
                component: SearchClassicComponent
            },
            {
                path: SearchModernComponent.Name,
                component: SearchModernComponent
            }
        ]
    };
}
