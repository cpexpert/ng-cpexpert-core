export * from './search.module';
export * from './classic/search-classic.component';
export * from './classic/search-classic.service';
export * from './modern/search-modern.component';
export * from './modern/search-modern.service';
