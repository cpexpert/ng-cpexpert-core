import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { SearchClassicService } from './search-classic.service';

@Component({
    selector     : 'search-classic',
    templateUrl  : './search-classic.component.html',
    styleUrls    : ['./search-classic.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class SearchClassicComponent implements OnInit, OnDestroy {
    public static readonly Name = 'classic';
    public searchItems: any;

    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(
        private _searchClassicService: SearchClassicService
    )
    {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    ngOnInit(): void
    {
        this._searchClassicService.dataOnChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(searchItems => {
                this.searchItems = searchItems;
            });
    }

    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
}
