import { NgModule } from '@angular/core';
import { ErrorsModule } from './errors/errors.module';
import { AuthModule } from './auth/auth.module';
import { SearchModule } from './search/search.module';
import { Route, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
    declarations: [],
    imports: [
        TranslateModule.forChild()
    ],
    exports: [
        AuthModule,
        ErrorsModule,
        SearchModule,
    ],
    providers: [
    ]
})
export class PublicPagesModule {
    public static Routes: Route[] = [
        ErrorsModule.Routes,
        AuthModule.Routes,
        SearchModule.Routes
    ];
}
