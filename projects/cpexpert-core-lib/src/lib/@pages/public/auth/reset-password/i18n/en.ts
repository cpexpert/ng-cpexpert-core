import { LanguagesEnum } from '../../../../../@common/enums/languages.enum';

export const locale = {
    lang: LanguagesEnum.English,
    data: {
        ResetPasswordPage : {
            Title: 'Reset your password',
            Email: 'Email',
            Password: 'Password',
            PasswordConfirm: 'Password (Confirm)',
            ResetMyPassword: 'Reset my password',
            GoBackToLoginPage: 'Go back to login page',
            Validations: {
                EmailIsRequired: 'Email is required',
                PleaseEnterAValidEmailAddress: 'Please enter a valid email address',
                PasswordIsRequired: 'Password is required',
                PasswordConfirmationIsRequired: 'Password confirmation is required',
                PasswordsMustMatch: 'Passwords must match',
            }
        }
    }
};
