import { LanguagesEnum } from '../../../../../@common/enums/languages.enum';

export const locale = {
    lang: LanguagesEnum.Polish,
    data: {
        ResetPasswordPage : {
            Title: 'Zresetuj hasło',
            Email: 'Adres e-mail',
            Password: 'Nowe',
            PasswordConfirm: 'Powtórz hasło',
            ResetMyPassword: 'Zresetuj',
            GoBackToLoginPage: 'Wróć do strony logowania',
            Validations: {
                EmailIsRequired: 'Adres e-mail jest wymagany',
                PleaseEnterAValidEmailAddress: 'Prosze wprowadzić poprawny adres e-mail',
                PasswordIsRequired: 'Hasło jest wymagane',
                PasswordConfirmationIsRequired: 'Powtórz hasło',
                PasswordsMustMatch: 'Wprowadzone hasła się nie są takie same',
            }
        }
    }
};
