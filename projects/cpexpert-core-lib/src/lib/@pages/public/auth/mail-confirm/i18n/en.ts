import { LanguagesEnum } from '../../../../../@common/enums/languages.enum';

export const locale = {
    lang: LanguagesEnum.English,
    data: {
        MailConfirmPage : {
            Title: 'Confirm your email address!',
            EmailHasBeenSentTo: 'A confirmation e-mail has been sent to',
            CheckYourInboxInstruction: 'Check your inbox and click on the "Confirm my email" link to confirm your email address',
            GoBackToLoginPage: 'Go back to login page',
        },
    },
};
