import { LanguagesEnum } from '../../../../../@common/enums/languages.enum';

export const locale = {
    lang: LanguagesEnum.Polish,
    data: {
        MailConfirmPage : {
            Title: 'Potwierdź swój adres e-mail!',
            EmailHasBeenSentTo: 'Wiadomość weryfikująca zsostała pomyślnie wysłana na adres',
            CheckYourInboxInstruction: 'Sprawdź swoją skrzynke e-mail, następnie postepuj zgodnie z instrukcją zawartą w przesłanej wiadomości',
            GoBackToLoginPage: 'Wróć do strony logowania',
        },
    }
};
