import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { FuseConfigService } from '../../../../@fuse/services/config.service';
import { fuseAnimations } from '../../../../@fuse/animations';
import { locale as english } from './i18n/en';
import { locale as polish } from './i18n/pl';
import { FuseTranslationLoaderService } from '../../../../@fuse/services/translation-loader.service';
import { AuthModule } from '../auth.module';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'mail-confirm',
    templateUrl: './mail-confirm.component.html',
    styleUrls: ['./mail-confirm.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class MailConfirmComponent implements OnInit {
    public static readonly Name = 'mail-confirm';
    public readonly LoginPagePath = AuthModule.NavigationLinks.LoginPage;
    public RegisterEmail = '';
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private _activatedRoute: ActivatedRoute 
    ) {
        this._fuseConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer: {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };

        this._fuseTranslationLoaderService.loadTranslations(english, polish);
    }

    ngOnInit(): void {
        this.RegisterEmail = this._activatedRoute.snapshot.paramMap.get('email');
    }
}
