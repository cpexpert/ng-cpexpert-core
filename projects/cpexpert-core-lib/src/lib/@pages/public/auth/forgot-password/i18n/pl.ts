import { LanguagesEnum } from '../../../../../@common/enums/languages.enum';

export const locale = {
    lang: LanguagesEnum.Polish,
    data: {
        ForgotPasswordPage : {
            Title: 'Odzyskaj hasło',
            Email: 'Twój e-mail',
            SendResetLink: 'Resetuj hasło',
            GoBackToLoginPage: 'Wróć do strony logowania',
            Validations: {
                EmailIsRequired: 'Adres e-mail jest wymagany',
                PleaseEnterAValidEmailAddress: 'Prosze wprowadzić poprawny adres e-mail',
            },
        },
    }
};
