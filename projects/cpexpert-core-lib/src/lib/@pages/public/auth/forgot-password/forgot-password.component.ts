import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FuseConfigService } from '../../../../@fuse/services/config.service';
import { fuseAnimations } from '../../../../@fuse/animations';
import { FuseTranslationLoaderService } from '../../../../@fuse/services/translation-loader.service';
import { locale as english } from './i18n/en';
import { locale as polish } from './i18n/pl';
import { AuthModule } from '../auth.module';

@Component({
    selector: 'forgot-password',
    templateUrl: './forgot-password.component.html',
    styleUrls: ['./forgot-password.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations    
})
export class ForgotPasswordComponent implements OnInit {
    public static readonly Name = 'forgot-password';
    public readonly LoginPagePath = AuthModule.NavigationLinks.LoginPage;
    public forgotPasswordForm: FormGroup;
    constructor(private _fuseConfigService: FuseConfigService, 
                private _formBuilder: FormBuilder,
                private _fuseTranslationLoaderService: FuseTranslationLoaderService
    ) {
        this._fuseConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer: {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };

        this._fuseTranslationLoaderService.loadTranslations(english, polish);
    }

    ngOnInit(): void {
        this.forgotPasswordForm = this._formBuilder.group({
            email: ['', [Validators.required, Validators.email]]
        });
    }
}
