import { LanguagesEnum } from '../../../../../@common/enums/languages.enum';

export const locale = {
    lang: LanguagesEnum.English,
    data: {
        ForgotPasswordPage: {
            Title: 'Password recovery',
            Email: 'Your e-mail',
            SendResetLink: 'Send reset link',
            GoBackToLoginPage: 'Go back to login page',
            Validations: {
                EmailIsRequired: 'Email is required',
                PleaseEnterAValidEmailAddress: 'Please enter a valid email address',
            },
        },
    }
};
