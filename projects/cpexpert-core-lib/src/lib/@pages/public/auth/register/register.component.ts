import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { FuseConfigService } from '../../../../@fuse/services/config.service';
import { fuseAnimations } from '../../../../@fuse/animations';
import { locale as english } from './i18n/en';
import { locale as polish } from './i18n/pl';
import { FuseTranslationLoaderService } from '../../../../@fuse/services/translation-loader.service';
import { AuthModule } from '../auth.module';
import { AuthService } from '../../../../@common/services/http/auth.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { takeUntil } from 'rxjs/operators';
import { Location } from '@angular-material-extensions/google-maps-autocomplete';
import PlaceResult = google.maps.places.PlaceResult;

@Component({
    selector: 'register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class RegisterComponent implements OnInit, OnDestroy {
    public static readonly Name = 'register';
    public readonly LoginPagePath = AuthModule.NavigationLinks.LoginPage;
    public registerForm: FormGroup;
    public _unsubscribeAll: Subject<any>;
    public _formValidationError = '';
    constructor(private _fuseConfigService: FuseConfigService,
                private _formBuilder: FormBuilder,
                private _fuseTranslationLoaderService: FuseTranslationLoaderService,
                private _authService: AuthService,
                private _router: Router,
                private _toastrService: ToastrService,
                private _translate: TranslateService
    ) {
        this._fuseConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer: {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };

        this._fuseTranslationLoaderService.loadTranslations(english, polish);
        this._unsubscribeAll = new Subject();
    }
    ngOnInit(): void {

        this._translate.get('RegisterPage.Errors.NoValid').subscribe((text: string) => { this._formValidationError = text; });

        this.registerForm = this._formBuilder.group({
            name: ['', Validators.required],
            address: ['', Validators.required],
            email: ['', [Validators.required, Validators.email]],
            password: ['', Validators.required],
            passwordConfirm: ['', [Validators.required, confirmPasswordValidator]],
            readAndAccept: ['', Validators.required],
        });

        this.registerForm.get('password').valueChanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
                this.registerForm.get('passwordConfirm').updateValueAndValidity();
            });
    }

    ngOnDestroy(): void {
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    onRegisterButtonClickHandler(): void {
        if (this.registerForm.valid) {
            const requestData = {
                email: this.registerForm.controls.email.value,
                password: this.registerForm.controls.password.value,
                passwordConfirm: this.registerForm.controls.passwordConfirm.value,
                readAndAccept: this.registerForm.controls.readAndAccept.value
            };

            this._authService.register(requestData).subscribe((response) => {
                if (response.IsSuccess) {
                    this._router.navigateByUrl(AuthModule.NavigationLinks.MailConfirmPage + '/' + requestData.email);
                }
            });
        } else {
            this._toastrService.warning(this._formValidationError);
        }
    }

    onAutocompleteSelected(result: PlaceResult) {
        console.log('onAutocompleteSelected: ', result);
    }
     
    onLocationSelected(location: Location) {
        console.log('onLocationSelected: ', location);
    }
}
export const confirmPasswordValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

    if (!control.parent || !control) {
        return null;
    }

    const password = control.parent.get('password');
    const passwordConfirm = control.parent.get('passwordConfirm');

    if (!password || !passwordConfirm) {
        return null;
    }

    if (passwordConfirm.value === '') {
        return null;
    }

    if (password.value === passwordConfirm.value) {
        return null;
    }

    return { passwordsNotMatching: true };
};
