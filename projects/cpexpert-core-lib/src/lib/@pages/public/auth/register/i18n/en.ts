import { LanguagesEnum } from '../../../../../@common/enums/languages.enum';

export const locale = {
    lang: LanguagesEnum.English,
    data: {
        RegisterPage : {
            Title: 'Register a new organization',
            Name: 'Name',
            Address: 'Office address',
            Email: 'Admin Email',
            Password: 'Password',
            PasswordConfirm: 'Password (Confirm)',
            IReadAndAccept: 'I read and accept',
            TermsAndConditions: 'Terms and conditions',
            CreateAnAccount: 'Create',
            AlreadyHaveAnAccount: 'Already have an access',
            ToLoginPage: 'Login page',
            Validations: {
                NameIsRequired: 'Organization name is required',
                AddressIsRequired: 'Address is required',
                EmailIsRequired: 'Email is required',
                PleaseEnterAValidEmailAddress: 'Please enter a valid email address',
                PasswordIsRequired: 'Password is required',
                PasswordConfirmationIsRequired: 'Password confirmation is required',
                PasswordsMustMatch: 'Passwords must match',
            },
            Errors: {
                NoValid: 'Validation error, can\'t register user.'
            }
        }
    }
};
