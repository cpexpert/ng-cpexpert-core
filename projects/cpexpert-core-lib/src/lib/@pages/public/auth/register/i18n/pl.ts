import { LanguagesEnum } from '../../../../../@common/enums/languages.enum';

export const locale = {
    lang: LanguagesEnum.Polish,
    data: {
        RegisterPage : {
            Title: 'Zarejestruj nową organizację',
            Name: 'Nazwa',
            Address: 'Adres biura',
            Email: 'Głowny adres e-mail',
            Password: 'Hasło',
            PasswordConfirm: 'Powtórz hasło',
            IReadAndAccept: 'Przeczytałem i akceptuje',
            TermsAndConditions: 'regulamin',
            CreateAnAccount: 'Rejestruj',
            AlreadyHaveAnAccount: 'Posiadasz już dostęp',
            ToLoginPage: 'Strona logowania',
            Validations: {
                NameIsRequired: 'Nazwa organizacji jest wymagana',
                AddressIsRequired: 'Adres biura jest wymagany',
                EmailIsRequired: 'Adres e-mail jest wymagany',
                PleaseEnterAValidEmailAddress: 'Prosze wprowadzić poprawny adres e-mail',
                PasswordIsRequired: 'Hasło jest wymagane',
                PasswordConfirmationIsRequired: 'Powtórz hasło',
                PasswordsMustMatch: 'Wprowadzone hasła się nie są takie same',
            },
            Errors: {
                NoValid: 'Błąd walidacji, nie można zarejestrować organizacji'
            }
        }
    }
};
