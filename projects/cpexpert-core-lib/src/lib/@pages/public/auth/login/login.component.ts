import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FuseConfigService } from '../../../../@fuse/services/config.service';
import { fuseAnimations } from '../../../../@fuse/animations';
import { FuseTranslationLoaderService } from '../../../../@fuse/services/translation-loader.service';
import { locale as english } from './i18n/en';
import { locale as polish } from './i18n/pl';
import { AuthModule } from '../auth.module';
import { AuthService } from '../../../../@common/services/http/auth.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { Utils } from '../../../../@common/utils';

@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class LoginComponent implements OnInit {
    public static readonly Name = 'login';
    public readonly ForgotPasswordPagePath = AuthModule.NavigationLinks.ForgotPasswordPage;
    public readonly RegisterPagePath = AuthModule.NavigationLinks.RegisterPage;
    public loginForm: FormGroup;
    private _formValidationError = '';
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private _authService: AuthService,
        private _toastrService: ToastrService,
        private _translate: TranslateService    ) {
        this._fuseConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer: {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };

        this._fuseTranslationLoaderService.loadTranslations(english, polish);
    }

    onLoginButtonClickHandler(): void {

        if (this.loginForm && this.loginForm.valid && this.loginForm.controls && this.loginForm.controls.email && this.loginForm.controls.password) {
            this._authService.login({
                Email: Utils.Encrypt(this.loginForm.controls.email.value),
                Password: Utils.Encrypt(this.loginForm.controls.password.value),
            }, this.loginForm.controls.rememberUser.value);
        } else {
            this._toastrService.warning(this._formValidationError);
        }
    }

    ngOnInit(): void {
        this.loginForm = this._formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', Validators.required],
            rememberUser: [false]
        });

        this._translate.get('LoginPage.Errors.NoValid').subscribe((text: string) => { this._formValidationError = text; });
    }
}
