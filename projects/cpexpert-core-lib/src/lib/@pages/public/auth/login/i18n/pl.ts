import { LanguagesEnum } from '../../../../../@common/enums/languages.enum';

export const locale = {
    lang: LanguagesEnum.Polish,
    data: {
        LoginPage : {
            Title: 'Zaloguj się',
            Email: 'Email',
            Password: 'Hasło',
            RememberMe: 'Zapamiętaj mnie',
            Login: 'Zaloguj',
            Or: 'Lub',
            LoginWithGoogle: 'Zaloguj przez Google',
            LoginWithFacebook: 'Zaloguj przez Facebook',
            DontHaveAnAccount: 'Nie posiadasz dostępu',
            ForgotPassword: 'Zapomniałeś hasła',
            CreateAnAccount: 'Zarejestruj nową organizację w systemie',
            Validations: {
                EmailIsRequired: 'Adres e-mail jest wymagany',
                PleaseEnterAValidEmailAddress: 'Wprowadź poprawny adres e-mail',
                PasswordIsRequired: 'Hasło jest wymagane',
            },
            Errors: {
                NoValid: 'Błąd walidacji, nie można zalogować użytkownika',
            }
        }
    }
};
