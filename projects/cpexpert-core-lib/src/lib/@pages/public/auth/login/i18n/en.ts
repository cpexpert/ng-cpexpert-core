import { LanguagesEnum } from '../../../../../@common/enums/languages.enum';

export const locale = {
    lang: LanguagesEnum.English,
    data: {
        LoginPage : {
            Title: 'Login to Your Account',
            Email: 'Email',
            Password: 'Password',
            RememberMe: 'Remember Me',
            Login: 'Login',
            Or: 'Or',
            LoginWithGoogle: 'Login with Google',
            LoginWithFacebook: 'Login with Facebook',
            DontHaveAnAccount: 'Don\'t have an access',
            ForgotPassword: 'Forgot password',
            CreateAnAccount: 'Create an new organization account',
            Validations: {
                EmailIsRequired: 'Email is required',
                PleaseEnterAValidEmailAddress: 'Please enter a valid Email address',
                PasswordIsRequired: 'Password is required',
            },
            Errors: {
                NoValid: 'Can\'t found user',
            }
        }
    }
};
