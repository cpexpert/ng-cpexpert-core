export * from './auth.module';
export * from './forgot-password/forgot-password.component';
export * from './lock/lock.component';
export * from './login/login.component';
export * from './mail-confirm/mail-confirm.component';
export * from './register/register.component';
export * from './reset-password/reset-password.component';
