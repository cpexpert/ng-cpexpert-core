import { LanguagesEnum } from '../../../../../@common/enums/languages.enum';

export const locale = {
    lang: LanguagesEnum.Polish,
    data: {
        LockPage : {
            Title: 'Twoja sesja wygasła',
            EnterYourPasswordToContinue: 'Z powodu braku aktywności Twoja sesja wygasła. Wprowadź hasło, aby kontynuować',
            Username: 'E-mail',
            Password: 'Hasło',
            Validations: {
                PasswordIsRequired: 'Hasło jest wymagane',
            },
            Unlock: 'Odblokuj',
            AreYouNot: 'Nie jesteś'
        },
    },
};
