import { LanguagesEnum } from '../../../../../@common/enums/languages.enum';

export const locale = {
    lang: LanguagesEnum.English,
    data: {
        LockPage : {
            Title: 'Your session is locked',
            EnterYourPasswordToContinue: 'Due to inactivity, your session is locked. Enter your password to continue',
            Username: 'Username',
            Password: 'Password',
            Validations: {
                PasswordIsRequired: 'Password is required',
            },
            Unlock: 'Unlock',
            AreYouNot: 'Are you not'
        },
    }
};
