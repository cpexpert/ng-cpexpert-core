import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FuseConfigService } from '../../../../@fuse/services/config.service';
import { fuseAnimations } from '../../../../@fuse/animations';
import { FuseTranslationLoaderService } from '../../../../@fuse/services/translation-loader.service';
import { locale as english } from './i18n/en';
import { locale as polish } from './i18n/pl';
import { AuthModule } from '../auth.module';

@Component({
    selector: 'lock',
    templateUrl: './lock.component.html',
    styleUrls: ['./lock.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class LockComponent implements OnInit {
    public static readonly Name = 'lock';
    public readonly LoginPagePath = AuthModule.NavigationLinks.LoginPage;
    public readonly LockedUserName = 'Oskar';
    lockForm: FormGroup;
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        private _fuseTranslationLoaderService: FuseTranslationLoaderService
    ) {
        this._fuseConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer: {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };
        
        this._fuseTranslationLoaderService.loadTranslations(english, polish);
    }
    ngOnInit(): void {
        this.lockForm = this._formBuilder.group({
            username: [
                {
                    value: 'Katherine',
                    disabled: true
                }, Validators.required
            ],
            password: ['', Validators.required]
        });
    }
}
