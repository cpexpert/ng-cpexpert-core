import { NgModule } from '@angular/core';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { LockComponent } from './lock/lock.component';
import { LoginComponent } from './login/login.component';
import { MailConfirmComponent } from './mail-confirm/mail-confirm.component';
import { RegisterComponent } from './register/register.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { FuseSharedModule } from '../../../@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { RouterModule } from '@angular/router';
import { MatGoogleMapsAutocompleteModule } from '@angular-material-extensions/google-maps-autocomplete';

@NgModule({
    declarations: [
        ForgotPasswordComponent,
        LockComponent,
        LoginComponent,
        MailConfirmComponent,
        RegisterComponent,
        ResetPasswordComponent
    ],
    imports: [
        RouterModule,
        MatButtonModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        FuseSharedModule,
        MatCheckboxModule,
        TranslateModule.forChild(),
        MatGoogleMapsAutocompleteModule
    ],
    exports: [
        ForgotPasswordComponent,
        LockComponent,
        LoginComponent,
        MailConfirmComponent,
        RegisterComponent,
        ResetPasswordComponent
    ],
    providers: [
    ]
})
export class AuthModule {
    static BasePathElement = 'auth';
    static  NavigationLinks = {
        ForgotPasswordPage: '/' + AuthModule.BasePathElement + '/' + ForgotPasswordComponent.Name,
        LockPage: '/' + AuthModule.BasePathElement + '/' + LockComponent.Name,
        LoginPage: '/' + AuthModule.BasePathElement + '/' + LoginComponent.Name,
        MailConfirmPage: '/' + AuthModule.BasePathElement + '/' + MailConfirmComponent.Name,
        RegisterPage: '/' + AuthModule.BasePathElement + '/' + RegisterComponent.Name,
        ResetPasswordPage: '/' + AuthModule.BasePathElement + '/' + ResetPasswordComponent.Name
    };
    static Routes =
    {
        path: AuthModule.BasePathElement,
        children: [
            {
                path: ForgotPasswordComponent.Name,
                component: ForgotPasswordComponent
            },
            {
                path: LockComponent.Name,
                component: LockComponent
            },
            {
                path: LoginComponent.Name,
                component: LoginComponent
            },
            {
                path: MailConfirmComponent.Name + '/:email',
                component: MailConfirmComponent
            },
            {
                path: RegisterComponent.Name,
                component: RegisterComponent
            },
            {
                path: ResetPasswordComponent.Name,
                component: ResetPasswordComponent
            }
        ]
    };
}
