import { InjectionToken } from '@angular/core';

export const ENVIRONMENT_SETTINGS = new InjectionToken('environment-settings');
