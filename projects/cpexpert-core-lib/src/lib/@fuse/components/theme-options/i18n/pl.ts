import { LanguagesEnum } from '../../../../@common/enums/languages.enum';

export const locale = {
    lang: LanguagesEnum.Polish,
    data: {
        ThemeOptionsComponent: {
            Title: 'Opcje wyglądu',
            ColorThemesBox: {
                Title: 'Kolor',
                DefaultLight: 'Domyślny jasny',
                YellowLight: 'Jasny żóły',
                BlueGrayDark: 'Ciemny niebiesko-szary',
                PinkDark: 'Ciemny różowy',
            },
            LayoutStylesBox: {
                Title: 'Układy',
                VerticalLayout1: 'Pionowy układ #1',
                VerticalLayout2: 'Pionowy układ #2',
                VerticalLayout3: 'Pionowy układ #3',
                HorizontalLayout1: 'Poziomy układ #1',
            },
            LayoutWidthBox: {
                Title: 'Szerokość',
                Fullwidth: 'Pełna',
                Boxed: 'Niepełna',
            },
            Navbar: {
                Title: 'Nawigacja',
                Hide: 'Ukryj',
                Folded: 'Zwinięty',
                Position: {
                    Title: 'Pozycja:',
                    Left: 'Po lewej',
                    Right: 'Po prawej',                
                },
                Variant: {
                    Title: 'Wariant:',
                    Style1: 'Styl #1',
                    Style2: 'Styl #2',
                },
                PrimaryBackground: {
                    Title: 'Kolor tła #1:',
                },
                SecondaryBackground: {
                    Title: 'Kolor tła #2:',
                }
            },
            Toolbar: {
                Title: 'Belka górna',
                Hide: 'Ukryj',
                Position: {
                    Title: 'Pozycja:',
                    Above: 'Powyżej',
                    BelowStatic: 'Powyżej statycznie',
                    BelowFixed: 'Powyżej dopasowane',
                    UseCustomBackgroundColor: 'Użyj niestandardowego koloru tła',
                },
                BackgroundColor: {
                    Title: 'Background color:',
                },
            },
            Footer: {
                Title: 'Stopka',
                Hide: 'Ukryj',
                Position: {
                    Title: 'Pozycja:',
                    Above: 'Powyżej',
                    BelowStatic: 'Powyżej statycznie',
                    BelowFixed: 'Powyżej dopasowane',
                    UseCustomBackgroundColor: 'Użyj niestandardowego koloru tła',
                },
                Color: {
                    Title: 'Kolor tła:',
                },
            },
            SidePanel: {
                Title: 'Panel boczny',
                Hide: 'Ukryj',
                Position: {
                    Title: 'Pozycja:',
                    Left: 'Po lewej',
                    Right: 'Po prawej',
                },
            },
            CustomScrollbars: {
                Title: 'Niestandardowy pasek przewijania',
                EnableCustomScrollbars: 'Włącz',
            },
        },
    }
};
