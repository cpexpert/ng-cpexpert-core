import { LanguagesEnum } from '../../../../@common/enums/languages.enum';

export const locale = {
    lang: LanguagesEnum.English,
    data: {
        ThemeOptionsComponent: {
            Title: 'Theme options2',
            ColorThemesBox: {
                Title: 'Color themes',
                DefaultLight: 'Default Light',
                YellowLight: 'Yellow Light',
                BlueGrayDark: 'Blue-Gray Dark',
                PinkDark: 'Pink Dark',
            },
            LayoutStylesBox: {
                Title: 'Layout Styles',
                VerticalLayout1: 'Vertical Layout #1',
                VerticalLayout2: 'Vertical Layout #2',
                VerticalLayout3: 'Vertical Layout #3',
                HorizontalLayout1: 'Horizontal Layout #1',
            },
            LayoutWidthBox: {
                Title: 'Layout Width',
                Fullwidth: 'Fullwidth',
                Boxed: 'Boxed',
            },
            Navbar: {
                Title: 'Navbar',
                Hide: 'Hide',
                Folded: 'Folded',
                Position: {
                    Title: 'Position:',
                    Left: 'Left',
                    Right: 'Right',                
                },
                Variant: {
                    Title: 'Variant:',
                    Style1: 'Style 1',
                    Style2: 'Style 2',
                },
                PrimaryBackground: {
                    Title: 'Primary background:',
                },
                SecondaryBackground: {
                    Title: 'Secondary background:',
                }
            },
            Toolbar: {
                Title: 'Toolbar',
                Hide: 'Hide',
                Position: {
                    Title: 'Position:',
                    Above: 'Above',
                    BelowStatic: 'Below Static',
                    BelowFixed: 'Below Fixed',
                    UseCustomBackgroundColor: 'Use custom background color',
                },
                BackgroundColor: {
                    Title: 'Background color:',
                },
            },
            Footer: {
                Title: 'Footer',
                Hide: 'Hide',
                Position: {
                    Title: 'Position:',
                    Above: 'Above',
                    BelowStatic: 'Below Static',
                    BelowFixed: 'Below Fixed',
                    UseCustomBackgroundColor: 'Use custom background color',
                },
                Color: {
                    Title: 'Color:',
                },
            },
            SidePanel: {
                Title: 'Side Panel',
                Hide: 'Hide',
                Position: {
                    Title: 'Position:',
                    Left: 'Left',
                    Right: 'Right',
                },
            },
            CustomScrollbars: {
                Title: 'Custom scrollbars',
                EnableCustomScrollbars: 'Enable custom strollbars',
            },
        },
    }
};
