import { LanguagesEnum } from '../../../../@common/enums/languages.enum';

export const locale = {
    lang: LanguagesEnum.English,
    data: {
        Shortcuts: {
            ApplicationShortcuts: 'Application or Pages shortcuts',
            NoShortcutsYet: 'No shortcuts or pages yet!',
            SearchForAnAppOrAPage: 'Search for an app or a page',
        },
    }
};
