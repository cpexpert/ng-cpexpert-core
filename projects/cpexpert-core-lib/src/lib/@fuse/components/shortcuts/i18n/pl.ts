import { LanguagesEnum } from '../../../../@common/enums/languages.enum';

export const locale = {
    lang: LanguagesEnum.Polish,
    data: {
        Shortcuts: {
            ApplicationShortcuts: 'Skróty aplikacji lub stron',
            NoShortcutsYet: 'Brak aplikacji lub strony',
            SearchForAnAppOrAPage: 'Szukaj aplikacji lub strony',
        },
    }
};
