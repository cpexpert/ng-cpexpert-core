import { LanguagesEnum } from '../../../../@common/enums/languages.enum';

export const locale = {
    lang: LanguagesEnum.English,
    data: {
        SearchBar: {
            Search: 'Search...',
        },
    }
};
