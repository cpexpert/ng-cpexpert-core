import { Routes, RouterModule, Router } from '@angular/router';
import { NgModule } from '@angular/core';
import { NGXLogger } from 'ngx-logger';
import { AuthModule, ErrorsModule, AuthService, ProfileModule, SignalRService, PublicPagesModule, PrivatePagesModule, CpExpertCoreModule } from 'cpexpert-core-lib';
const routes: Routes = [
  { path: '',   redirectTo: AuthModule.NavigationLinks.LoginPage, pathMatch: 'full' },
  { path: '**', redirectTo: ErrorsModule.NavigationLinks.Error404Page, pathMatch: 'full' },
];

@NgModule({
  imports: [
    RouterModule.forChild(CpExpertCoreModule.Routes),
    RouterModule.forRoot(routes),
  ],
  exports: [RouterModule]
})
export class AppCoreRoutingModule {
    constructor(private _router: Router, private _logger: NGXLogger, private _authService: AuthService, private _signalRService: SignalRService) {
      this._logger.debug('Loaded routes:', this._router.config);
      this._authService.loginSuccessEvent.subscribe((user) => {
        this._logger.debug('LOGIN ', user);
        this._signalRService.startConnection();
        this._router.navigateByUrl(ProfileModule.NavigationLinks.ProfilePage);
      });
      this._authService.logoutSuccessEvent.subscribe((userAppKey) => {
        this._logger.debug('LOGOUT ', userAppKey);
        this._signalRService.destroy();
        this._router.navigateByUrl(AuthModule.NavigationLinks.LoginPage);
      });
    }
}
