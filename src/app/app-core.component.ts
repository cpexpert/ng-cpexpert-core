import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { Subject } from 'rxjs';
import { DOCUMENT } from '@angular/common';
import { Platform } from '@angular/cdk/platform';
import { NGXLogger } from 'ngx-logger';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import {
    FuseConfigService, FuseSidebarService,
    FuseSplashScreenService, AuthService, SignalRService,
    LocalStorageKeysEnum, Utils, ErrorsModule, SettingService
} from 'cpexpert-core-lib';

@Component({
    selector: 'app',
    templateUrl: './app-core.component.html',
    styleUrls: ['./app-core.component.scss']
})
export class AppCoreComponent implements OnInit, OnDestroy {

    fuseConfig: any;

    private _unsubscribeAll: Subject<any> = new Subject();

    constructor(
        @Inject(DOCUMENT) private document: any,
        private _fuseConfigService: FuseConfigService,
        private _fuseSidebarService: FuseSidebarService,
        private _fuseSplashScreenService: FuseSplashScreenService,
        private _platform: Platform,
        private _logger: NGXLogger,
        private _router: Router,
        private _authService: AuthService,
        private _signalRService: SignalRService,
        private _settingsService: SettingService
    ) {
        try {
            this._unsubscribeAll = new Subject();
            this._fuseSplashScreenService.show();
            if (this._platform.ANDROID || this._platform.IOS) {
                this.document.body.classList.add('is-mobile');
            }

            let currentFrontendEncryptedKey = Utils.LocalStorage.Get(LocalStorageKeysEnum.FrontendEncryptedKey);
            if (!currentFrontendEncryptedKey) {
                this._settingsService.getFrontendEncryptedKey().subscribe((response) => {
                    if (response.IsSuccess && response.ResponseData && response.ResponseData.Value) {
                        Utils.LocalStorage.Set(LocalStorageKeysEnum.FrontendEncryptedKey, response.ResponseData.Value);
                        currentFrontendEncryptedKey = Utils.LocalStorage.Get(LocalStorageKeysEnum.FrontendEncryptedKey);
                    }

                    if (!currentFrontendEncryptedKey) {
                        this.errorOnInitHandle('No frontend encrypted key on server!');
                        return;
                    }

                    this._authService.tryAutoLogin();
                    this._logger.info('Application starteded...');
                });
            } else {
                this._authService.tryAutoLogin();
                this._logger.info('Application starteded...');
            }
        } catch (error) {
            this.errorOnInitHandle(error);
        }
    }

    ngOnInit(): void {
        this._fuseConfigService.config
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((config) => {

                this.fuseConfig = config;
                if (this.fuseConfig.layout.width === 'boxed') {
                    this.document.body.classList.add('boxed');
                }
                else {
                    this.document.body.classList.remove('boxed');
                }

                const max = this.document.body.classList.length;
                for (let i = 0; i < max; i++) {
                    const className = this.document.body.classList[i];
                    if (className.startsWith('theme-')) {
                        this.document.body.classList.remove(className);
                    }
                }

                this.document.body.classList.add(this.fuseConfig.colorTheme);
            });
    }

    ngOnDestroy(): void {
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
        this._signalRService.destroy();
        this._logger.info('Application closed...');
    }

    toggleSidebarOpen(key: any): void {
        this._fuseSidebarService.getSidebar(key).toggleOpen();
    }

    private errorOnInitHandle(error: any): void {
        this._logger.info('Application stopped with error...');
        this._logger.error(error);
        this._router.navigateByUrl(ErrorsModule.NavigationLinks.Error500Page, {
            state: {
                backButtonShow: false,
                registerErrorButtonShow: false,
            }
        });
    }
}
