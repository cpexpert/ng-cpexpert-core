import { NgxLoggerLevel, LoggerModule, NGXLogger } from 'ngx-logger';
import { NgModule } from '@angular/core';
import { AppCoreComponent } from './app-core.component';
import { AppCoreRoutingModule } from './app-core-routing.module';
import { BrowserModule, HAMMER_LOADER } from '@angular/platform-browser';
import { ToastrModule } from 'ngx-toastr';
import { HttpClientModule } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { CpExpertCoreModule, ENVIRONMENT_SETTINGS } from 'cpexpert-core-lib';
import { environment } from 'src/environments/environment';
import { AgmCoreModule } from '@agm/core';
import { MatGoogleMapsAutocompleteModule } from '@angular-material-extensions/google-maps-autocomplete';

const loggerSettings = !environment.production ? 
    { level: NgxLoggerLevel.DEBUG } : 
    { serverLoggingUrl: environment.HttpApiServices.Core + '/logs', level: NgxLoggerLevel.DEBUG, serverLogLevel: NgxLoggerLevel.ERROR };

@NgModule({
    declarations: [
        AppCoreComponent
    ],
    imports: [
        AppCoreRoutingModule,
        LoggerModule.forRoot(loggerSettings),
        BrowserModule,
        BrowserAnimationsModule,
        ToastrModule.forRoot(),
        HttpClientModule,
        MatMomentDateModule,
        MatButtonModule,
        MatIconModule,
        CpExpertCoreModule,
        MatGoogleMapsAutocompleteModule,
        TranslateModule.forRoot(),
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyAb8uu4JL0xXt1pmUi6BKCI9DXyRhnMtvY',
            libraries: ['places', 'drawing', 'geometry', 'visualization', 'geocoder']
        }),
    ],
    providers: [
        { provide: ENVIRONMENT_SETTINGS, useValue: environment },
        { provide: HAMMER_LOADER, useValue: () => new Promise(() => {}) }
    ],
    bootstrap: [
        AppCoreComponent
    ]
})
export class AppCoreModule {
    constructor(private _logger: NGXLogger) {
        this._logger.debug('Loaded environment:', environment);
    }
}