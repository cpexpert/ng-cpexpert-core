export const environment = {
    production: false,
    baseAppUrl: 'http://localhost:4200',
    HttpApiServices: {
        Core: 'http://localhost:5001/api',
        SignalR: 'http://localhost:5002/api'
    }
};
