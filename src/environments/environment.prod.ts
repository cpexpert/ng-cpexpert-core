export const environment = {
    production: true,
    baseAppUrl: 'http://cp-expert.pl:82',
    HttpApiServices: {
        Core: 'http://cp-expert.pl/tests/CPEXPERT/CoreApi/api',
        SignalR: 'http://cp-expert.pl/tests/CPEXPERT/SignalRApi/api'
    }
};
